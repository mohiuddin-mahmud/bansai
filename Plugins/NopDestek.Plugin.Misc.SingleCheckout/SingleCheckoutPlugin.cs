﻿using System.Web.Routing;
using System.Linq;
using Nop.Core.Plugins;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Topics;
using Nop.Core;
using Nop.Services.Configuration;

namespace NopDestek.Plugin.Misc.SingleCheckout
{
    public class SingleCheckoutPlugin : BasePlugin, IMiscPlugin, IPlugin
    {
        private readonly ITopicService _topicService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IWebHelper _webHelper;
        private readonly ISettingService _settingService;

        public SingleCheckoutPlugin(
            IWorkContext workContext,
            IWebHelper webHelper,
            ILocalizationService localizationService,
            ITopicService topicService,
            ISettingService settingService)
        {
            this._workContext = workContext;
            this._webHelper = webHelper;
            this._localizationService = localizationService;
            this._topicService = topicService;
            this._settingService = settingService;
        }


        public override void Install()
        {
            var setting = new SingleCheckoutSettings()
            {
                AllowSavedAddress = true,
            };
            _settingService.SaveSetting(setting);

            this.InstallStringResources();
            base.Install();
        }

        public override void Uninstall()
        {
            //locales
            var en = _localizationService.GetAllResources(_workContext.WorkingLanguage.Id)
                .Where(r => r.ResourceName.StartsWith(string.Format("Plugins.{0}.", PluginDescriptor.SystemName)));
            //locales
            foreach (var item in en)
            {
                _localizationService.DeleteLocaleStringResource(item);
            }
            base.Uninstall();
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "SingleCheckout";
            routeValues = new RouteValueDictionary() { { "Namespaces", "NopDestek.Plugin.Misc.SingleCheckout.Controllers" }, { "area", null } };
        }

        
    }
}
