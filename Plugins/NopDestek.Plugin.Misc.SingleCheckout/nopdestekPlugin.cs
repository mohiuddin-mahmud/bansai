﻿using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Core.Plugins;
using Nop.Services.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NopDestek.Plugin.Misc.SingleCheckout
{
    public static class nopdestekPlugin
    {
        private static IWebHelper _webHelper { get { return EngineContext.Current.Resolve<IWebHelper>(); } }
        private static IWorkContext _workContext { get { return EngineContext.Current.Resolve<IWorkContext>(); } }
        private static ILocalizationService _localizationService { get { return EngineContext.Current.Resolve<ILocalizationService>(); } }

        public static void InstallStringResources(this BasePlugin plugin)
        {
            string path = CommonHelper.MapPath(string.Format("~/Plugins/{0}/Localization", plugin.PluginDescriptor.SystemName)),
                lang = _workContext.WorkingLanguage.LanguageCulture.ToLower(),
                filePath = System.IO.Path.Combine(path, string.Format("resources.{0}.xml", lang));
            string file = System.IO.File.Exists(filePath) ? filePath : System.IO.Path.Combine(path, "resources.en-us.xml");
            string xml = System.IO.File.ReadAllText(file);

            _localizationService.ImportResourcesFromXml(_workContext.WorkingLanguage, xml);
        }
    }
}
