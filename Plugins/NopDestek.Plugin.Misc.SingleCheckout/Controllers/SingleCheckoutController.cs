﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Web.Extensions;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Checkout;
using Nop.Web.Models.Common;
using NopDestek.Plugin.Misc.SingleCheckout.Models;
using Nop.Core.Infrastructure;
using Nop.Services.Configuration;
using Nop.Core.Data;
using Nop.Web.Controllers;
using Nop.Web.Framework.Themes;
using Nop.Core.Plugins;
using Nop.Web.Models.ShoppingCart;
using Nop.Services.Seo;
using Nop.Core.Domain.Catalog;
using Nop.Services.Discounts;
using Nop.Services.Security;
using Nop.Services.Stores;
using System.Globalization;
using Nop.Services.Media;
using Nop.Core.Domain.Media;
using Nop.Web.Models.Media;
using Nop.Web.Infrastructure.Cache;
using Nop.Core.Caching;
using Nop.Core.Domain.Tax; 
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Factories;

namespace NopDestek.Plugin.Misc.SingleCheckout
{
    [NopHttpsRequirement(SslRequirement.Yes)]
    public partial class SingleCheckoutController : BasePublicController
    {
        #region Fields
        private readonly IAddressModelFactory _addressModelFactory;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly ILocalizationService _localizationService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IShippingService _shippingService;
        private readonly IPaymentService _paymentService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IRewardPointService _rewardPointService;
        private readonly IAddressAttributeService _addressAttributeService;
        private readonly IAddressAttributeParser _addressAttributeParser;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IThemeContext _themeContext;
        private readonly ILogger _logger;
        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly HttpContextBase _httpContext;
        private readonly OrderSettings _orderSettings;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly PaymentSettings _paymentSettings;
        private readonly AddressSettings _addressSettings;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly SingleCheckoutSettings _singleCheckoutSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly TaxSettings _taxSettings;
        private readonly ISettingService _settingService;
        private readonly IAddressService _addressService;
        private readonly IRepository<Address> _addressRepository;
        private readonly IPluginFinder _pluginFinder;
        private readonly ICheckoutAttributeParser _checkoutAttributeParser;
        private readonly ICheckoutAttributeFormatter _checkoutAttributeFormatter;
        private readonly IDiscountService _discountService;
        private readonly ICheckoutAttributeService _checkoutAttributeService;
        private readonly IPermissionService _permissionService;
        private readonly IDownloadService _downloadService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ICacheManager _cacheManager;
        private readonly IPictureService _pictureService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IGiftCardService _giftCardService;

        #endregion

        public string PluginName { get { return "Misc.SingleCheckout"; } }

        public bool Installed
        {
            get
            {
                var pl = _pluginFinder.GetPluginDescriptorBySystemName<SingleCheckoutPlugin>(PluginName);
                return pl != null;
            }
        }

        #region Constructors

        public SingleCheckoutController(IStoreMappingService storeMappingService, IAddressModelFactory addressModelFactory ,IWorkContext workContext,
            IShoppingCartService shoppingCartService, ILocalizationService localizationService,
            ITaxService taxService, ICurrencyService currencyService,
            IPriceFormatter priceFormatter, IOrderProcessingService orderProcessingService,
            ICustomerService customerService, IGenericAttributeService genericAttributeService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService, IShippingService shippingService,
            IPaymentService paymentService, IOrderTotalCalculationService orderTotalCalculationService,
            IAddressAttributeService addressAttributeService,
            IAddressAttributeParser addressAttributeParser,
            IAddressAttributeFormatter addressAttributeFormatter,
            IThemeContext themeContext,
            ILogger logger, IOrderService orderService, IWebHelper webHelper,
            HttpContextBase httpContext,
            OrderSettings orderSettings, RewardPointsSettings rewardPointsSettings,
            PaymentSettings paymentSettings, AddressSettings addressSettings,
            IStoreContext storeContext, SingleCheckoutSettings singleCheckoutSettings,
            ShippingSettings shippingSettings,
            IPluginFinder pluginFinder,
            ISettingService settingService, IAddressService addressService, IRepository<Address> addressRepository,
            IRewardPointService rewardPointService,
            ShoppingCartSettings shoppingCartSettings,
            CatalogSettings catalogSettings,
            MediaSettings mediaSettings,
            TaxSettings taxSettings,
            ICheckoutAttributeParser checkoutAttributeParser,
            ICheckoutAttributeFormatter checkoutAttributeFormatter,
            IDiscountService discountService,
            ICheckoutAttributeService checkoutAttributeService,
            IPermissionService permissionService,
            IDownloadService downloadService,
            IPriceCalculationService priceCalculationService,
            ICacheManager cacheManager,
            IPictureService pictureService,
            IProductAttributeService productAttributeService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            IGiftCardService giftCardService)
        {
            this._storeMappingService = storeMappingService;
            this._addressModelFactory = addressModelFactory;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._shoppingCartService = shoppingCartService;
            this._localizationService = localizationService;
            this._taxService = taxService;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._orderProcessingService = orderProcessingService;
            this._customerService = customerService;
            this._genericAttributeService = genericAttributeService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._shippingService = shippingService;
            this._paymentService = paymentService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._logger = logger;
            this._orderService = orderService;
            this._addressAttributeService = addressAttributeService;
            this._addressAttributeParser = addressAttributeParser;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._themeContext = themeContext;
            this._pluginFinder = pluginFinder;
            this._webHelper = webHelper;
            this._httpContext = httpContext;
            this._orderSettings = orderSettings;
            this._shippingSettings = shippingSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._paymentSettings = paymentSettings;
            this._addressSettings = addressSettings;
            this._singleCheckoutSettings = singleCheckoutSettings;
            this._settingService = settingService;
            this._addressService = addressService;
            this._addressRepository = addressRepository;
            this._rewardPointService = rewardPointService;
            this._shoppingCartSettings = shoppingCartSettings;
            this._catalogSettings = catalogSettings;
            this._checkoutAttributeParser = checkoutAttributeParser;
            this._checkoutAttributeFormatter = checkoutAttributeFormatter;
            this._discountService = discountService;
            this._checkoutAttributeService = checkoutAttributeService;
            this._permissionService = permissionService;
            this._downloadService = downloadService;
            this._priceCalculationService = priceCalculationService;
            this._mediaSettings = mediaSettings;
            this._cacheManager = cacheManager;
            this._pictureService = pictureService;
            this._productAttributeService = productAttributeService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._productAttributeParser = productAttributeParser;
            this._giftCardService = giftCardService;
            this._taxSettings = taxSettings;
        }

        #endregion


        #region Utilities

        public ActionResult InstallStringResources()
        {
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(PluginName);
            if (pluginDescriptor == null)
                return Content("Error finding plugin descriptior");

            var plugin = pluginDescriptor.Instance() as SingleCheckoutPlugin;
            if (plugin == null)
                return Content("Error finding plugin");

            plugin.InstallStringResources();

            return RedirectToAction("ConfigureMiscPlugin", "Plugin", new System.Web.Routing.RouteValueDictionary() { { "Namespaces", "Nop.Admin.Controllers" }, { "systemName", PluginName } });
        }
        [NonAction]
        protected virtual void PrepareShoppingCartModel(ShoppingCartModel model,
            IList<ShoppingCartItem> cart, bool isEditable = true,
            bool validateCheckoutAttributes = false,
            bool prepareEstimateShippingIfEnabled = true, bool setEstimateShippingDefaultAddress = true,
            bool prepareAndDisplayOrderReviewData = false)
        {
            if (cart == null)
                throw new ArgumentNullException("cart");

            if (model == null)
                throw new ArgumentNullException("model");

            model.OnePageCheckoutEnabled = _orderSettings.OnePageCheckoutEnabled;

            if (cart.Count == 0)
                return;

            #region Simple properties

            model.IsEditable = isEditable;
            model.ShowProductImages = _shoppingCartSettings.ShowProductImagesOnShoppingCart;
            model.ShowSku = _catalogSettings.ShowSkuOnProductDetailsPage; // ShowProductSku;
            var checkoutAttributesXml = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
            model.CheckoutAttributeInfo = _checkoutAttributeFormatter.FormatAttributes(checkoutAttributesXml, _workContext.CurrentCustomer);
            bool minOrderSubtotalAmountOk = _orderProcessingService.ValidateMinOrderSubtotalAmount(cart);
            if (!minOrderSubtotalAmountOk)
            {
                decimal minOrderSubtotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderSubtotalAmount, _workContext.WorkingCurrency);
                model.MinOrderSubtotalWarning = string.Format(_localizationService.GetResource("Checkout.MinOrderSubtotalAmount"), _priceFormatter.FormatPrice(minOrderSubtotalAmount, true, false));
            }
            model.TermsOfServiceOnShoppingCartPage = _orderSettings.TermsOfServiceOnShoppingCartPage;
            model.TermsOfServiceOnOrderConfirmPage = _orderSettings.TermsOfServiceOnOrderConfirmPage;
            model.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoShoppingCart;
             
            //discount and gift card boxes 
            model.DiscountBox.Display = _shoppingCartSettings.ShowDiscountBox;
            var discountCouponCodes = _workContext.CurrentCustomer.ParseAppliedDiscountCouponCodes();
            foreach (var couponCode in discountCouponCodes)
            {
                var discount = _discountService.GetAllDiscountsForCaching(couponCode: couponCode)
                    .FirstOrDefault(d => d.RequiresCouponCode && _discountService.ValidateDiscount(d, _workContext.CurrentCustomer).IsValid);

                model.DiscountBox.AppliedDiscountsWithCodes.Add(new ShoppingCartModel.DiscountBoxModel.DiscountInfoModel()
                {
                    Id = discount.Id,
                    CouponCode = discount.CouponCode
                });
            }
            model.GiftCardBox.Display = _shoppingCartSettings.ShowGiftCardBox;
            //cart warnings
            var cartWarnings = _shoppingCartService.GetShoppingCartWarnings(cart, checkoutAttributesXml, validateCheckoutAttributes);
            foreach (var warning in cartWarnings)
                model.Warnings.Add(warning);

            #endregion

            #region Checkout attributes

            var checkoutAttributes = _checkoutAttributeService.GetAllCheckoutAttributes(_storeContext.CurrentStore.Id, !cart.RequiresShipping());
            foreach (var attribute in checkoutAttributes)
            {
                var attributeModel = new ShoppingCartModel.CheckoutAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.GetLocalized(x => x.Name),
                    TextPrompt = attribute.GetLocalized(x => x.TextPrompt),
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType,
                    DefaultValue = attribute.DefaultValue
                };
                if (!String.IsNullOrEmpty(attribute.ValidationFileAllowedExtensions))
                {
                    attributeModel.AllowedFileExtensions = attribute.ValidationFileAllowedExtensions
                        .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                        .ToList();
                }

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _checkoutAttributeService.GetCheckoutAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new ShoppingCartModel.CheckoutAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.GetLocalized(x => x.Name),
                            ColorSquaresRgb = attributeValue.ColorSquaresRgb,
                            IsPreSelected = attributeValue.IsPreSelected,
                        };
                        attributeModel.Values.Add(attributeValueModel);

                        //display price if allowed
                        if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                        {
                            decimal priceAdjustmentBase = _taxService.GetCheckoutAttributePrice(attributeValue);
                            decimal priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);
                            if (priceAdjustmentBase > decimal.Zero)
                                attributeValueModel.PriceAdjustment = "+" + _priceFormatter.FormatPrice(priceAdjustment);
                            else if (priceAdjustmentBase < decimal.Zero)
                                attributeValueModel.PriceAdjustment = "-" + _priceFormatter.FormatPrice(-priceAdjustment);
                        }
                    }
                }



                //set already selected attributes
                var selectedCheckoutAttributes = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.Checkboxes:
                    case AttributeControlType.ColorSquares:
                        {
                            if (!String.IsNullOrEmpty(selectedCheckoutAttributes))
                            {
                                //clear default selection
                                foreach (var item in attributeModel.Values)
                                    item.IsPreSelected = false;

                                //select new values
                                var selectedValues = _checkoutAttributeParser.ParseCheckoutAttributeValues(selectedCheckoutAttributes);
                                foreach (var attributeValue in selectedValues)
                                    foreach (var item in attributeModel.Values)
                                        if (attributeValue.Id == item.Id)
                                            item.IsPreSelected = true;
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //do nothing
                            //values are already pre-set
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            if (!String.IsNullOrEmpty(selectedCheckoutAttributes))
                            {
                                var enteredText = _checkoutAttributeParser.ParseValues(selectedCheckoutAttributes, attribute.Id);
                                if (enteredText.Count > 0)
                                    attributeModel.DefaultValue = enteredText[0];
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                        {
                            //keep in mind my that the code below works only in the current culture
                            var selectedDateStr = _checkoutAttributeParser.ParseValues(selectedCheckoutAttributes, attribute.Id);
                            if (selectedDateStr.Count > 0)
                            {
                                DateTime selectedDate;
                                if (DateTime.TryParseExact(selectedDateStr[0], "D", CultureInfo.CurrentCulture,
                                                       DateTimeStyles.None, out selectedDate))
                                {
                                    //successfully parsed
                                    attributeModel.SelectedDay = selectedDate.Day;
                                    attributeModel.SelectedMonth = selectedDate.Month;
                                    attributeModel.SelectedYear = selectedDate.Year;
                                }
                            }

                        }
                        break;
                    case AttributeControlType.FileUpload:
                        {
                            if (!String.IsNullOrEmpty(selectedCheckoutAttributes))
                            {
                                var downloadGuidStr = _checkoutAttributeParser.ParseValues(selectedCheckoutAttributes, attribute.Id).FirstOrDefault();
                                Guid downloadGuid;
                                Guid.TryParse(downloadGuidStr, out downloadGuid);
                                var download = _downloadService.GetDownloadByGuid(downloadGuid);
                                if (download != null)
                                    attributeModel.DefaultValue = download.DownloadGuid.ToString();
                            }
                        }
                        break;
                    default:
                        break;
                }

                model.CheckoutAttributes.Add(attributeModel);
            }

            #endregion 

            #region Estimate shipping

            if (prepareEstimateShippingIfEnabled)
            {
                model.EstimateShipping.Enabled = cart.Count > 0 && cart.RequiresShipping() && _shippingSettings.EstimateShippingEnabled;
                if (model.EstimateShipping.Enabled)
                {
                    //countries
                    int? defaultEstimateCountryId = (setEstimateShippingDefaultAddress && _workContext.CurrentCustomer.ShippingAddress != null) ? _workContext.CurrentCustomer.ShippingAddress.CountryId : model.EstimateShipping.CountryId;
                    model.EstimateShipping.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Address.SelectCountry"), Value = "0" });
                    foreach (var c in _countryService.GetAllCountriesForShipping(_workContext.WorkingLanguage.Id))
                        model.EstimateShipping.AvailableCountries.Add(new SelectListItem
                        {
                            Text = c.GetLocalized(x => x.Name),
                            Value = c.Id.ToString(),
                            Selected = c.Id == defaultEstimateCountryId
                        });
                    //states
                    int? defaultEstimateStateId = (setEstimateShippingDefaultAddress && _workContext.CurrentCustomer.ShippingAddress != null) ? _workContext.CurrentCustomer.ShippingAddress.StateProvinceId : model.EstimateShipping.StateProvinceId;
                    var states = defaultEstimateCountryId.HasValue ? _stateProvinceService.GetStateProvincesByCountryId(defaultEstimateCountryId.Value, _workContext.WorkingLanguage.Id).ToList() : new List<StateProvince>();
                    if (states.Count > 0)
                        foreach (var s in states)
                            model.EstimateShipping.AvailableStates.Add(new SelectListItem
                            {
                                Text = s.GetLocalized(x => x.Name),
                                Value = s.Id.ToString(),
                                Selected = s.Id == defaultEstimateStateId
                            });
                    else
                        model.EstimateShipping.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Address.OtherNonUS"), Value = "0" });

                    if (setEstimateShippingDefaultAddress && _workContext.CurrentCustomer.ShippingAddress != null)
                        model.EstimateShipping.ZipPostalCode = _workContext.CurrentCustomer.ShippingAddress.ZipPostalCode;
                }
            }

            #endregion

            #region Cart items

            foreach (var sci in cart)
            {
                var cartItemModel = new ShoppingCartModel.ShoppingCartItemModel
                {
                    Id = sci.Id,
                    Sku = sci.Product.FormatSku(sci.AttributesXml, _productAttributeParser),
                    ProductId = sci.Product.Id,
                    ProductName = sci.Product.GetLocalized(x => x.Name),
                    ProductSeName = sci.Product.GetSeName(),
                    Quantity = sci.Quantity,
                    AttributeInfo = _productAttributeFormatter.FormatAttributes(sci.Product, sci.AttributesXml),
                };

                //allow editing?
                //1. setting enabled?
                //2. simple product?
                //3. has attribute or gift card?
                //4. visible individually?
                cartItemModel.AllowItemEditing = _shoppingCartSettings.AllowCartItemEditing &&
                    sci.Product.ProductType == ProductType.SimpleProduct &&
                    (!String.IsNullOrEmpty(cartItemModel.AttributeInfo) || sci.Product.IsGiftCard) &&
                    sci.Product.VisibleIndividually;

                //allowed quantities
                var allowedQuantities = sci.Product.ParseAllowedQuantities();
                foreach (var qty in allowedQuantities)
                {
                    cartItemModel.AllowedQuantities.Add(new SelectListItem
                    {
                        Text = qty.ToString(),
                        Value = qty.ToString(),
                        Selected = sci.Quantity == qty
                    });
                }

                //recurring info
                if (sci.Product.IsRecurring)
                    cartItemModel.RecurringInfo = string.Format(_localizationService.GetResource("ShoppingCart.RecurringPeriod"), sci.Product.RecurringCycleLength, sci.Product.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));

                //rental info
                if (sci.Product.IsRental)
                {
                    var rentalStartDate = sci.RentalStartDateUtc.HasValue ? sci.Product.FormatRentalDate(sci.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = sci.RentalEndDateUtc.HasValue ? sci.Product.FormatRentalDate(sci.RentalEndDateUtc.Value) : "";
                    cartItemModel.RentalInfo = string.Format(_localizationService.GetResource("ShoppingCart.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }

                //unit prices
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.UnitPrice = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    decimal taxRate;
                    decimal shoppingCartUnitPriceWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetUnitPrice(sci), out taxRate);
                    decimal shoppingCartUnitPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartUnitPriceWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.UnitPrice = _priceFormatter.FormatPrice(shoppingCartUnitPriceWithDiscount);
                }
                //subtotal, discount
                if (sci.Product.CallForPrice)
                {
                    cartItemModel.SubTotal = _localizationService.GetResource("Products.CallForPrice");
                }
                else
                {
                    //sub total
                    List<DiscountForCaching> scDiscounts;
                    int? maximumDiscountQty;
                    decimal shoppingCartItemDiscountBase;
                    decimal taxRate;
                    decimal shoppingCartItemSubTotalWithDiscountBase = _taxService.GetProductPrice(sci.Product, _priceCalculationService.GetSubTotal(sci, true, out shoppingCartItemDiscountBase, out scDiscounts, out maximumDiscountQty), out taxRate);
                    decimal shoppingCartItemSubTotalWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemSubTotalWithDiscountBase, _workContext.WorkingCurrency);
                    cartItemModel.SubTotal = _priceFormatter.FormatPrice(shoppingCartItemSubTotalWithDiscount);

                    //display an applied discount amount
                    if (shoppingCartItemDiscountBase > decimal.Zero)
                    {
                        shoppingCartItemDiscountBase = _taxService.GetProductPrice(sci.Product, shoppingCartItemDiscountBase, out taxRate);
                        if (shoppingCartItemDiscountBase > decimal.Zero)
                        {
                            decimal shoppingCartItemDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartItemDiscountBase, _workContext.WorkingCurrency);
                            cartItemModel.Discount = _priceFormatter.FormatPrice(shoppingCartItemDiscount);
                        }
                    } 
                }

                //picture
                if (_shoppingCartSettings.ShowProductImagesOnShoppingCart)
                {
                    cartItemModel.Picture = PrepareCartItemPictureModel(sci,
                        _mediaSettings.CartThumbPictureSize, true, cartItemModel.ProductName);
                }

                //item warnings
                var itemWarnings = _shoppingCartService.GetShoppingCartItemWarnings(
                    _workContext.CurrentCustomer,
                    sci.ShoppingCartType,
                    sci.Product,
                    sci.StoreId,
                    sci.AttributesXml,
                    sci.CustomerEnteredPrice,
                    sci.RentalStartDateUtc,
                    sci.RentalEndDateUtc,
                    sci.Quantity,
                    false);
                foreach (var warning in itemWarnings)
                    cartItemModel.Warnings.Add(warning);

                model.Items.Add(cartItemModel);
            }

            #endregion

            #region Button payment methods 

            var paymentMethods = _paymentService
                .LoadActivePaymentMethods(_workContext.CurrentCustomer, _storeContext.CurrentStore.Id)
                .Where(pm => pm.PaymentMethodType == PaymentMethodType.Button)
                .Where(pm => !pm.HidePaymentMethod(cart))
                .ToList();
            foreach (var pm in paymentMethods)
            {
                if (cart.IsRecurring() && pm.RecurringPaymentType == RecurringPaymentType.NotSupported)
                    continue;

                string actionName;
                string controllerName;
                RouteValueDictionary routeValues;
                pm.GetPaymentInfoRoute(out actionName, out controllerName, out routeValues);

                model.ButtonPaymentMethodActionNames.Add(actionName);
                model.ButtonPaymentMethodControllerNames.Add(controllerName);
                model.ButtonPaymentMethodRouteValues.Add(routeValues);
            }

            #endregion

            #region Order review data
            //order review data 
            if (prepareAndDisplayOrderReviewData)
            {
                
                var model2 = new ShoppingCartModel.OrderReviewDataModel();

                model.OrderReviewData.Display = true;
                //billing info
                var billingAddress = _workContext.CurrentCustomer.BillingAddress;
                if (billingAddress != null)
                {
                    _addressModelFactory.PrepareAddressModel(model2.BillingAddress,
                            address: billingAddress,
                            excludeProperties: false,
                            addressSettings: _addressSettings);
                }
               
                //shipping info
                if (cart.RequiresShipping())
                {
                    model2.IsShippable = true;

                    var pickupPoint = _workContext.CurrentCustomer.GetAttribute<PickupPoint>(SystemCustomerAttributeNames.SelectedPickupPoint, _storeContext.CurrentStore.Id);
                    model2.SelectedPickUpInStore = _shippingSettings.AllowPickUpInStore && pickupPoint != null;
                    if (!model2.SelectedPickUpInStore)
                    {
                        if (_workContext.CurrentCustomer.ShippingAddress != null)
                        {
                            _addressModelFactory.PrepareAddressModel(model2.ShippingAddress,
                                address: _workContext.CurrentCustomer.ShippingAddress,
                                excludeProperties: false,
                                addressSettings: _addressSettings);
                        }
                    }
                    else
                    {
                        var country = _countryService.GetCountryByTwoLetterIsoCode(pickupPoint.CountryCode);
                        var state = _stateProvinceService.GetStateProvinceByAbbreviation(pickupPoint.StateAbbreviation);
                        model2.PickupAddress = new AddressModel
                        {
                            Address1 = pickupPoint.Address,
                            City = pickupPoint.City,
                            CountryName = country != null ? country.Name : string.Empty,
                            StateProvinceName = state != null ? state.Name : string.Empty,
                            ZipPostalCode = pickupPoint.ZipPostalCode
                        };
                    }


                    //selected shipping method
                    var shippingOption = _workContext.CurrentCustomer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                    if (shippingOption != null)
                        model.OrderReviewData.ShippingMethod = shippingOption.Name;
                }
                //payment info
                var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod, _storeContext.CurrentStore.Id);
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
                model.OrderReviewData.PaymentMethod = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id) : "";

                //custom values
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest != null)
                {
                    model.OrderReviewData.CustomValues = processPaymentRequest.CustomValues;
                }
            }
            #endregion
        }

        [NonAction]
        protected virtual PictureModel PrepareCartItemPictureModel(ShoppingCartItem sci,
            int pictureSize, bool showDefaultPicture, string productName)
        {
            var pictureCacheKey = string.Format(ModelCacheEventConsumer.CART_PICTURE_MODEL_KEY, sci.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
            var model = _cacheManager.Get(pictureCacheKey,
                //as we cache per user (shopping cart item identifier)
                //let's cache just for 3 minutes
                3, () =>
                {
                    //shopping cart item picture
                    var sciPicture = sci.Product.GetProductPicture(sci.AttributesXml, _pictureService, _productAttributeParser);
                    return new PictureModel
                    {
                        ImageUrl = _pictureService.GetPictureUrl(sciPicture, pictureSize, showDefaultPicture),
                        Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), productName),
                        AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), productName),
                    };
                });
            return model;
        }

        [NonAction]
        protected virtual OrderTotalsModel PrepareOrderTotalsModel(IList<ShoppingCartItem> cart, bool isEditable)
        {
            var model = new OrderTotalsModel();
            model.IsEditable = isEditable;

            if (cart.Count > 0)
            {
                //subtotal
                decimal orderSubTotalDiscountAmountBase;
                List<DiscountForCaching> orderSubTotalAppliedDiscount;
                decimal subTotalWithoutDiscountBase;
                decimal subTotalWithDiscountBase;
                var subTotalIncludingTax = _workContext.TaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal;
                _orderTotalCalculationService.GetShoppingCartSubTotal(cart, subTotalIncludingTax,
                    out orderSubTotalDiscountAmountBase, out orderSubTotalAppliedDiscount,
                    out subTotalWithoutDiscountBase, out subTotalWithDiscountBase);
                decimal subtotalBase = subTotalWithoutDiscountBase;
                decimal subtotal = _currencyService.ConvertFromPrimaryStoreCurrency(subtotalBase, _workContext.WorkingCurrency);
                model.SubTotal = _priceFormatter.FormatPrice(subtotal, true, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);

                if (orderSubTotalDiscountAmountBase > decimal.Zero)
                {
                    decimal orderSubTotalDiscountAmount = _currencyService.ConvertFromPrimaryStoreCurrency(orderSubTotalDiscountAmountBase, _workContext.WorkingCurrency);
                    model.SubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountAmount, true, _workContext.WorkingCurrency, _workContext.WorkingLanguage, subTotalIncludingTax);
                }
                 


                //shipping info
                model.RequiresShipping = cart.RequiresShipping();
                if (model.RequiresShipping)
                {
                    decimal? shoppingCartShippingBase = _orderTotalCalculationService.GetShoppingCartShippingTotal(cart);
                    if (shoppingCartShippingBase.HasValue)
                    {
                        decimal shoppingCartShipping = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartShippingBase.Value, _workContext.WorkingCurrency);
                        model.Shipping = _priceFormatter.FormatShippingPrice(shoppingCartShipping, true);

                        //selected shipping method
                        var shippingOption = _workContext.CurrentCustomer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                        if (shippingOption != null)
                            model.SelectedShippingMethod = shippingOption.Name;
                    }
                }

                //payment method fee
                var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod, _storeContext.CurrentStore.Id);
                decimal paymentMethodAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, paymentMethodSystemName);
                decimal paymentMethodAdditionalFeeWithTaxBase = _taxService.GetPaymentMethodAdditionalFee(paymentMethodAdditionalFee, _workContext.CurrentCustomer);
                if (paymentMethodAdditionalFeeWithTaxBase > decimal.Zero)
                {
                    decimal paymentMethodAdditionalFeeWithTax = _currencyService.ConvertFromPrimaryStoreCurrency(paymentMethodAdditionalFeeWithTaxBase, _workContext.WorkingCurrency);
                    model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeWithTax, true);
                }

                //tax
                bool displayTax = true;
                bool displayTaxRates = true;
                if (_taxSettings.HideTaxInOrderSummary && _workContext.TaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    displayTax = false;
                    displayTaxRates = false;
                }
                else
                {
                    SortedDictionary<decimal, decimal> taxRates;
                    decimal shoppingCartTaxBase = _orderTotalCalculationService.GetTaxTotal(cart, out taxRates);
                    decimal shoppingCartTax = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartTaxBase, _workContext.WorkingCurrency);

                    if (shoppingCartTaxBase == 0 && _taxSettings.HideZeroTax)
                    {
                        displayTax = false;
                        displayTaxRates = false;
                    }
                    else
                    {
                        displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Count > 0;
                        displayTax = !displayTaxRates;

                        model.Tax = _priceFormatter.FormatPrice(shoppingCartTax, true, false);
                        foreach (var tr in taxRates)
                        {
                            model.TaxRates.Add(new OrderTotalsModel.TaxRate
                            {
                                Rate = _priceFormatter.FormatTaxRate(tr.Key),
                                Value = _priceFormatter.FormatPrice(_currencyService.ConvertFromPrimaryStoreCurrency(tr.Value, _workContext.WorkingCurrency), true, false),
                            });
                        }
                    }
                }
                model.DisplayTaxRates = displayTaxRates;
                model.DisplayTax = displayTax;

                //total
                decimal orderTotalDiscountAmountBase;
                List<DiscountForCaching> orderTotalAppliedDiscount;
                List<AppliedGiftCard> appliedGiftCards;
                int redeemedRewardPoints;
                decimal redeemedRewardPointsAmount;
                decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart,
                    out orderTotalDiscountAmountBase, out orderTotalAppliedDiscount,
                    out appliedGiftCards, out redeemedRewardPoints, out redeemedRewardPointsAmount);
                if (shoppingCartTotalBase.HasValue)
                {
                    decimal shoppingCartTotal = _currencyService.ConvertFromPrimaryStoreCurrency(shoppingCartTotalBase.Value, _workContext.WorkingCurrency);
                    model.OrderTotal = _priceFormatter.FormatPrice(shoppingCartTotal, true, false);
                }

                //discount
                if (orderTotalDiscountAmountBase > decimal.Zero)
                {
                    decimal orderTotalDiscountAmount = _currencyService.ConvertFromPrimaryStoreCurrency(orderTotalDiscountAmountBase, _workContext.WorkingCurrency);
                    model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderTotalDiscountAmount, true, false);
                }
                 

                //gift cards
                if (appliedGiftCards != null && appliedGiftCards.Count > 0)
                {
                    foreach (var appliedGiftCard in appliedGiftCards)
                    {
                        var gcModel = new OrderTotalsModel.GiftCard
                        {
                            Id = appliedGiftCard.GiftCard.Id,
                            CouponCode = appliedGiftCard.GiftCard.GiftCardCouponCode,
                        };
                        decimal amountCanBeUsed = _currencyService.ConvertFromPrimaryStoreCurrency(appliedGiftCard.AmountCanBeUsed, _workContext.WorkingCurrency);
                        gcModel.Amount = _priceFormatter.FormatPrice(-amountCanBeUsed, true, false);

                        decimal remainingAmountBase = appliedGiftCard.GiftCard.GetGiftCardRemainingAmount() - appliedGiftCard.AmountCanBeUsed;
                        decimal remainingAmount = _currencyService.ConvertFromPrimaryStoreCurrency(remainingAmountBase, _workContext.WorkingCurrency);
                        gcModel.Remaining = _priceFormatter.FormatPrice(remainingAmount, true, false);

                        model.GiftCards.Add(gcModel);
                    }
                }

                //reward points to be spent (redeemed)
                if (redeemedRewardPointsAmount > decimal.Zero)
                {
                    decimal redeemedRewardPointsAmountInCustomerCurrency = _currencyService.ConvertFromPrimaryStoreCurrency(redeemedRewardPointsAmount, _workContext.WorkingCurrency);
                    model.RedeemedRewardPoints = redeemedRewardPoints;
                    model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-redeemedRewardPointsAmountInCustomerCurrency, true, false);
                }

                //reward points to be earned
                if (_rewardPointsSettings.Enabled &&
                    _rewardPointsSettings.DisplayHowMuchWillBeEarned &&
                    shoppingCartTotalBase.HasValue)
                {
                    model.WillEarnRewardPoints = _orderTotalCalculationService
                        .CalculateRewardPoints(_workContext.CurrentCustomer, shoppingCartTotalBase.Value);
                }

            }

            return model;
        }
        private void ValidateZipPostalCode(int CountryId, int StateProvinceId, string ZipPostalCode, bool billingaddress)
        {
            if (_singleCheckoutSettings.ValidateAddress)
            {
                string message = _localizationService.GetResource("Plugins.Misc.SingleCheckout.ZipValidateAddress");
                int existZip = 0;
                try
                {
                    var context = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Data.IDbContext>();
                    var sql = "Select count(*) from [dbo].[TaxRate] where CountryId = {0} and Zip = {1} and StateProvinceId = {2}";
                    existZip = context.SqlQuery<int>(sql, CountryId, ZipPostalCode, StateProvinceId).First();
                    if (existZip == 0)
                    {
                        if (billingaddress)
                            ModelState.AddModelError("BillingNewAddress.ZipPostalCode", message);
                        else
                            ModelState.AddModelError("ShippingNewAddress.ZipPostalCode", message);
                    }
                }
                catch
                {
                    if (billingaddress)
                        ModelState.AddModelError("BillingNewAddress.ZipPostalCode", message);
                    else
                        ModelState.AddModelError("ShippingNewAddress.ZipPostalCode", message);
                }
            }
        }


        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            var model = new ConfigurationModel();
            model.AllowSavedAddress = _singleCheckoutSettings.AllowSavedAddress;
            model.ValidateAddress = _singleCheckoutSettings.ValidateAddress;
            model.OnePageCheckout = _orderSettings.OnePageCheckoutEnabled;
            return View("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/Configure.cshtml", model);

        }

        [HttpPost]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {

            //save settings
            _singleCheckoutSettings.AllowSavedAddress = model.AllowSavedAddress;
            _singleCheckoutSettings.ValidateAddress = model.ValidateAddress;
            _settingService.SaveSetting(_singleCheckoutSettings);
            _orderSettings.OnePageCheckoutEnabled = model.OnePageCheckout;
            _settingService.SaveSetting(_orderSettings);

            //redisplay the form
            return View("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/Configure.cshtml", model);
        }


        [NonAction]
        protected bool IsPaymentWorkflowRequired(IList<ShoppingCartItem> cart, bool ignoreRewardPoints = false)
        {
            bool result = true;

            //check whether order total equals zero
            decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart, ignoreRewardPoints);
            if (shoppingCartTotalBase.HasValue && shoppingCartTotalBase.Value == decimal.Zero)
                result = false;
            return result;
        }

        [NonAction]
        protected CheckoutBillingAddressModel PrepareBillingAddressModel(int? selectedCountryId = null)
        {
            var model = new CheckoutBillingAddressModel();
            var customer = _workContext.CurrentCustomer;

            //existing addresses
            var addresses = _workContext.CurrentCustomer.Addresses
                .Where(a => a.Country == null ||
                    (//published
                    a.Country.Published &&
                    //allow billing
                    a.Country.AllowsBilling &&
                    //enabled for the current store
                    _storeMappingService.Authorize(a.Country)))
                .ToList();
            foreach (var address in addresses)
            {
                var addressModel = new AddressModel();
                _addressModelFactory.PrepareAddressModel(addressModel,
                    address: address,
                    excludeProperties: false,
                    addressSettings: _addressSettings);
                model.ExistingAddresses.Add(addressModel);
            }

            //new address
            model.NewAddress.CountryId = selectedCountryId;
            _addressModelFactory.PrepareAddressModel(model.NewAddress,
                address: null,
                excludeProperties: false,
                addressSettings: _addressSettings,
                loadCountries: () => _countryService.GetAllCountriesForBilling(_workContext.WorkingLanguage.Id),
                prePopulateWithCustomerFields: false,
                customer: _workContext.CurrentCustomer,
                overrideAttributesXml: "");
            


            int storeId = 0;  //_storeContext.CurrentStore.Id;
            string firstname = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName, storeId);
            string lastname = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName, storeId);
            string company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company, storeId);
            int? countryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId, storeId);
            int? stateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId, storeId);

            string city = customer.GetAttribute<string>(SystemCustomerAttributeNames.City, storeId);
            string address1 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress, storeId);
            string address2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2, storeId);
            string zipcode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode, storeId);
            string phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone, storeId);
            string fax = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax, storeId);

            if (!String.IsNullOrEmpty(firstname))
                model.NewAddress.FirstName = firstname;

            if (!String.IsNullOrEmpty(lastname))
                model.NewAddress.LastName = lastname;

            if (!String.IsNullOrEmpty(company))
                model.NewAddress.Company = company;

            //if (addresses.Count() > 0)
            {
                if (countryId.HasValue)
                    model.NewAddress.CountryId = countryId.Value;

                if (stateProvinceId.HasValue)
                {
                    model.NewAddress.StateProvinceId = stateProvinceId.Value;

                    var states = _stateProvinceService
                            .GetStateProvincesByCountryId(countryId.HasValue ? countryId.Value : 0)
                            .ToList();
                    if (states.Where(x => x.Id == stateProvinceId.Value).FirstOrDefault() != null)
                    {
                        foreach (var s in states)
                        {
                            model.NewAddress.AvailableStates.Add(new SelectListItem()
                            {
                                Text = s.GetLocalized(x => x.Name),
                                Value = s.Id.ToString(),
                                Selected = (s.Id == stateProvinceId.Value)
                            });
                        }

                        model.NewAddress.StateProvinceName = states.Where(x => x.Id == stateProvinceId.Value).FirstOrDefault().Name;
                    }
                }
            }
            if (!String.IsNullOrEmpty(city))
                model.NewAddress.City = city;

            if (!String.IsNullOrEmpty(address1))
                model.NewAddress.Address1 = address1;

            if (!String.IsNullOrEmpty(address2))
                model.NewAddress.Address2 = address2;

            if (!String.IsNullOrEmpty(zipcode))
                model.NewAddress.ZipPostalCode = zipcode;

            if (!String.IsNullOrEmpty(phone))
                model.NewAddress.PhoneNumber = phone;

            if (!String.IsNullOrEmpty(fax))
                model.NewAddress.FaxNumber = fax;

            model.NewAddress.Email = _workContext.CurrentCustomer.Email;

            return model;
        }

        [NonAction]
        protected SingleCheckoutShippingAddressModel PrepareShippingAddressModel(int? selectedCountryId = null, bool sameassBA = true, bool savebilling = false, int idBillingAddress = 0)
        {
            var model = new SingleCheckoutShippingAddressModel();
            model.CanCheck = true;
            model.idBillingAddress = idBillingAddress;
            var addresses = _workContext.CurrentCustomer.Addresses.ToList();
            model.SameAsBillingAddress = sameassBA;
            var add = _workContext.CurrentCustomer.BillingAddress;
            if (add != null)
            {
                if (add.CountryId.HasValue)
                    if (!_countryService.GetCountryById(add.CountryId.Value).AllowsShipping)
                    {
                        model.SameAsBillingAddress = false;
                        model.CanCheck = false;
                    }
            }

            if (_singleCheckoutSettings.AllowSavedAddress == false)
            {
                int idcountry = _workContext.CurrentCustomer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId);
                if (idcountry != 0)
                    if (idcountry != 0)
                        if (_countryService.GetCountryById(idcountry).AllowsShipping)
                        {
                            model.SameAsBillingAddress = true;
                            model.CanCheck = true;
                        }
            }


            foreach (var address in addresses)
            {
                if (address.CountryId.HasValue)
                {
                    if (_countryService.GetCountryById(address.CountryId.Value).AllowsShipping)
                    {
                        var addressModel = new AddressModel();
                        _addressModelFactory.PrepareAddressModel(addressModel,
                            address: address,
                            excludeProperties: false,
                            addressSettings: _addressSettings);
                        model.ExistingAddresses.Add(addressModel);
                         
                    }
                }
                else
                {
                    var addressModel = new AddressModel();
                    _addressModelFactory.PrepareAddressModel(addressModel,
                        address: address,
                        excludeProperties: false,
                        addressSettings: _addressSettings);
                    model.ExistingAddresses.Add(addressModel);
                }
            }

            if (savebilling)
                if (model.ExistingAddresses.Count() == 0)
                {
                    model.SameAsBillingAddress = false;
                }

            if (add != null)
                if (model.ExistingAddresses.Count() == 0)
                    model.CanCheck = false;

            //new address
            model.NewAddress.CountryId = selectedCountryId;
            _addressModelFactory.PrepareAddressModel(model.NewAddress,
                address: null,
                excludeProperties: false,
                addressSettings: _addressSettings,
                loadCountries: () => _countryService.GetAllCountriesForShipping(_workContext.WorkingLanguage.Id),
                prePopulateWithCustomerFields: false,
                customer: _workContext.CurrentCustomer,
                overrideAttributesXml: "");
             
            return model;
        }

        [NonAction]
        protected CheckoutShippingMethodModel PrepareShippingMethodModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutShippingMethodModel();

            var getShippingOptionResponse = _shippingService.GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress);
            if (getShippingOptionResponse.Success)
            {
                //performance optimization. cache returned shipping options.
                //we'll use them later (after a customer has selected an option).
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.OfferedShippingOptions, getShippingOptionResponse.ShippingOptions);

                foreach (var shippingOption in getShippingOptionResponse.ShippingOptions)
                {
                    var soModel = new CheckoutShippingMethodModel.ShippingMethodModel()
                    {
                        Name = shippingOption.Name,
                        Description = shippingOption.Description,
                        ShippingRateComputationMethodSystemName = shippingOption.ShippingRateComputationMethodSystemName,
                    };

                    //adjust rate
                    List<DiscountForCaching> appliedDiscount = null;
                    var shippingTotal = _orderTotalCalculationService.AdjustShippingRate(
                        shippingOption.Rate, cart, out appliedDiscount);

                    decimal rateBase = _taxService.GetShippingPrice(shippingTotal, _workContext.CurrentCustomer);
                    decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                    soModel.Fee = _priceFormatter.FormatShippingPrice(rate, true);

                    model.ShippingMethods.Add(soModel);
                }

                //find a selected (previously) shipping method
                var lastShippingOption = _workContext.CurrentCustomer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                if (lastShippingOption != null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.ToList()
                        .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(lastShippingOption.Name, StringComparison.InvariantCultureIgnoreCase) &&
                        !String.IsNullOrEmpty(so.ShippingRateComputationMethodSystemName) && so.ShippingRateComputationMethodSystemName.Equals(lastShippingOption.ShippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                    if (shippingOptionToSelect != null)
                    {
                        shippingOptionToSelect.Selected = true;
                    }
                }
                //if no option has been selected, let's do it for the first one
                if (model.ShippingMethods.FirstOrDefault(so => so.Selected) == null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.FirstOrDefault();
                    if (shippingOptionToSelect != null)
                    {
                        shippingOptionToSelect.Selected = true;
                    }
                }

                //notify about shipping from multiple locations
                if (_shippingSettings.NotifyCustomerAboutShippingFromMultipleLocations)
                {
                    model.NotifyCustomerAboutShippingFromMultipleLocations = getShippingOptionResponse.ShippingFromMultipleLocations;
                }

            }
            else
                foreach (var error in getShippingOptionResponse.Errors)
                    model.Warnings.Add(error);

            return model;
        }

        [NonAction]
        protected CheckoutPaymentMethodModel PreparePaymentMethodModel(IList<ShoppingCartItem> cart, int filterByCountryId)
        {
            var model = new CheckoutPaymentMethodModel();

            //reward points
            if (_rewardPointsSettings.Enabled && !cart.IsRecurring())
            {
                int rewardPointsBalance = _rewardPointService.GetRewardPointsBalance(_workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id);
                decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
                decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);
                if (rewardPointsAmount > decimal.Zero &&
                    _orderTotalCalculationService.CheckMinimumRewardPointsToUseRequirement(rewardPointsBalance))
                {
                    model.DisplayRewardPoints = true;
                    model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);
                    model.RewardPointsBalance = rewardPointsBalance;
                }
            }
            //Nop.Services.Orders.IOrderTotalCalculationService _calcServ = EngineContext.Current.Resolve<Nop.Services.Orders.IOrderTotalCalculationService>();

            var boundPaymentMethods = _paymentService
                .LoadActivePaymentMethods(_workContext.CurrentCustomer, _storeContext.CurrentStore.Id, filterByCountryId)
                .Where(pm => pm.PaymentMethodType == PaymentMethodType.Standard || pm.PaymentMethodType == PaymentMethodType.Redirection)
                .Where(pm => !pm.HidePaymentMethod(cart))
                .ToList();
            foreach (var pm in boundPaymentMethods)
            {
                if (cart.IsRecurring() && pm.RecurringPaymentType == RecurringPaymentType.NotSupported)
                    continue;

                var pmModel = new CheckoutPaymentMethodModel.PaymentMethodModel()
                {
                    Name = pm.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id),
                    PaymentMethodSystemName = pm.PluginDescriptor.SystemName,
                };

                decimal paymentMethodAdditionalFee = 0;
                //decimal paymentMethodAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, pm.PluginDescriptor.SystemName);
                decimal rateBase = _taxService.GetPaymentMethodAdditionalFee(paymentMethodAdditionalFee, _workContext.CurrentCustomer);
                decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                if (rate > decimal.Zero)
                    pmModel.Fee = _priceFormatter.FormatPaymentMethodAdditionalFee(rate, true);

                model.PaymentMethods.Add(pmModel);
            }

            //find a selected (previously) payment method
            //find a selected (previously) payment method
            var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            if (!String.IsNullOrEmpty(selectedPaymentMethodSystemName))
            {
                var paymentMethodToSelect = model.PaymentMethods.ToList()
                    .Find(pm => pm.PaymentMethodSystemName.Equals(selectedPaymentMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }
            //if no option has been selected, let's do it for the first one
            if (model.PaymentMethods.Where(so => so.Selected).FirstOrDefault() == null)
            {
                var paymentMethodToSelect = model.PaymentMethods.FirstOrDefault();
                if (paymentMethodToSelect != null)
                {
                    paymentMethodToSelect.Selected = true;
                    _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.SelectedPaymentMethod, paymentMethodToSelect.PaymentMethodSystemName, _storeContext.CurrentStore.Id);
                }
            }

            return model;
        }

        [NonAction]
        protected CheckoutPaymentInfoModel PreparePaymentInfoModel(IPaymentMethod paymentMethod)
        {
            var model = new CheckoutPaymentInfoModel();
            string actionName;
            string controllerName;
            RouteValueDictionary routeValues;
            paymentMethod.GetPaymentInfoRoute(out actionName, out controllerName, out routeValues);
            model.PaymentInfoActionName = actionName;
            model.PaymentInfoControllerName = controllerName;
            model.PaymentInfoRouteValues = routeValues;
            model.DisplayOrderTotals = _orderSettings.OnePageCheckoutDisplayOrderTotalsOnPaymentInfoTab;
            return model;
        }

        [NonAction]
        protected CheckoutConfirmModel PrepareConfirmOrderModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutConfirmModel();
            //min order amount validation
            bool minOrderTotalAmountOk = _orderProcessingService.ValidateMinOrderTotalAmount(cart);
            if (!minOrderTotalAmountOk)
            {
                decimal minOrderTotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderTotalAmount, _workContext.WorkingCurrency);
                model.MinOrderTotalWarning = string.Format(_localizationService.GetResource("Checkout.MinOrderTotalAmount"), _priceFormatter.FormatPrice(minOrderTotalAmount, true, false));
            }
            return model;
        }

        [NonAction]
        protected bool UseOnePageCheckout()
        {
            return _orderSettings.OnePageCheckoutEnabled;
        }

        [NonAction]
        protected bool IsMinimumOrderPlacementIntervalValid(Customer customer)
        {
            if (_orderSettings.MinimumOrderPlacementInterval == 0)
                return true;

            var lastOrder = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id, customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                .FirstOrDefault();
            if (lastOrder == null)
                return true;

            var interval = DateTime.UtcNow - lastOrder.CreatedOnUtc;
            return interval.TotalSeconds > _orderSettings.MinimumOrderPlacementInterval;
        }

        #endregion


        #region Methods (one page checkout)

        public ActionResult OnePageCheckout()
        {
            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (!UseOnePageCheckout())
                return RedirectToRoute("Checkout");

            if (!Installed) {
                return RedirectToAction("OnePageCheckout", "Checkout");
            }
            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();


            var model = new NopDestek.Plugin.Misc.SingleCheckout.Models.OnePageCheckoutModel()
            {
                ShippingRequired = cart.RequiresShipping(),
            };

            //if(Session["onepagecheckouttrial"]!=null)
            //{
            //    Session["onepagecheckouttrial"] = Convert.ToInt32(Session["onepagecheckouttrial"]) + 1;
            //}
            //else
            //{
            //    Session["onepagecheckouttrial"] = 0;
            //}

            //if (Convert.ToInt32(Session["onepagecheckouttrial"]) > 2)
            //{
            //    Session["onepagecheckouttrial"] = 0;
            //    return View("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/TrialVersion.cshtml");                
            //}

            return View("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OnePageCheckout.cshtml", model);

        }

        [ChildActionOnly]
        public ActionResult OpcBillingForm()
        {
               var billingAddressModel = PrepareBillingAddressModel();
            return PartialView("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel);
        }

        [ChildActionOnly]
        public ActionResult OpcShippingForm()
        {
            var shippingAddressModel = PrepareShippingAddressModel();

            //return PartialView("OpcShippingAddress", shippingAddressModel);
            return PartialView("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel);
        }

        [ChildActionOnly]
        public ActionResult OpcShippingMethod()
        {
            //var cart = _workContext.CurrentCustomer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
               .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
               .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
               .ToList();
            if (cart.Count == 0)
                throw new Exception("Your cart is empty");

            if (!UseOnePageCheckout())
                throw new Exception("One page checkout is disabled");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                throw new Exception("Anonymous checkout is not allowed");

            if (!cart.RequiresShipping())
            {
                var model = new CheckoutShippingMethodModel();
                //return PartialView("OpcShippingMethods", model);
                return PartialView("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingMethods.cshtml", model);
            }
            //    throw new Exception("Shipping is not required");

            var shippingMethodModel = PrepareShippingMethodModel(cart);

            //return PartialView("OpcShippingMethods", shippingMethodModel);
            return PartialView("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingMethods.cshtml", shippingMethodModel);
        }

        [ChildActionOnly]
        public ActionResult OpcPaymentMethod()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
               .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
               .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
               .ToList();

            if (cart.Count == 0)
                throw new Exception("Your cart is empty");

            if (!UseOnePageCheckout())
                throw new Exception("One page checkout is disabled");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                throw new Exception("Anonymous checkout is not allowed");

            //filter by country
            int filterByCountryId = 0;
            if (_addressSettings.CountryEnabled &&
                _workContext.CurrentCustomer.BillingAddress != null &&
                _workContext.CurrentCustomer.BillingAddress.Country != null)
            {
                filterByCountryId = _workContext.CurrentCustomer.BillingAddress.Country.Id;
            }

            var paymentMethodModel = PreparePaymentMethodModel(cart, filterByCountryId);

            return PartialView("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcPaymentMethods.cshtml", paymentMethodModel);
        }

        [ChildActionOnly]
        public ActionResult OpcPaymentInfo()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
               .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
               .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
               .ToList();
            if (cart.Count == 0)
                throw new NopException("Your cart is empty");

            if (!UseOnePageCheckout())
                throw new NopException("One page checkout is disabled");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                throw new NopException("Anonymous checkout is not allowed");

            int filterByCountryId = 0;
            if (_addressSettings.CountryEnabled &&
                _workContext.CurrentCustomer.BillingAddress != null &&
                _workContext.CurrentCustomer.BillingAddress.Country != null)
            {
                filterByCountryId = _workContext.CurrentCustomer.BillingAddress.Country.Id;
            }

            var paymentMethodModel = PreparePaymentMethodModel(cart, filterByCountryId);

            var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName);
            if (paymentMethodInst == null || !paymentMethodInst.IsPaymentMethodActive(_paymentSettings))
                throw new NopException("Selected payment method can't be parsed");

            var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);

            if (!String.IsNullOrEmpty(selectedPaymentMethodSystemName))
            {
                paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
            }


            var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst);
            //return PartialView("OpcPaymentInfo", paymenInfoModel);
            return PartialView("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcPaymentInfo.cshtml", paymenInfoModel);

        }

        [ChildActionOnly]
        public ActionResult OpcOrderInfo()
        {
            //var cart = _workContext.CurrentCustomer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
            if (cart.Count == 0)
                throw new Exception("Your cart is empty");

            if (!UseOnePageCheckout())
                throw new Exception("One page checkout is disabled");

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                throw new Exception("Anonymous checkout is not allowed");

            //if (!cart.RequiresShipping())
            //    throw new Exception("Shipping is not required");

            if (String.IsNullOrEmpty(_workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id)))
            {
                var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions);
                if (shippingOptions != null)
                {
                    var shippingOption = shippingOptions.FirstOrDefault();
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);
                }
            }

            var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedPaymentMethod,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            if (String.IsNullOrEmpty(selectedPaymentMethodSystemName))
            {
                var paymentMethod = _paymentService.LoadActivePaymentMethods().FirstOrDefault();
                //_workContext.CurrentCustomer.SelectedPaymentMethodSystemName = paymentMethod.PluginDescriptor.SystemName;
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                               SystemCustomerAttributeNames.SelectedPaymentMethod,
                               selectedPaymentMethodSystemName, _storeContext.CurrentStore.Id);
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);
            }

            var confirmOrderModel = PrepareConfirmOrderModel(cart);

            //return PartialView("OpcConfirmOrder", confirmOrderModel);
            return PartialView("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcConfirmOrder.cshtml", confirmOrderModel);

        }

        [ValidateInput(false)]
        public ActionResult OpcChangeBillingAddress(FormCollection form)
        {
            try
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                int billingAddressId = 0;
                int.TryParse(form["billing_address_id"], out billingAddressId);

                var address = _workContext.CurrentCustomer.Addresses.Where(a => a.Id == billingAddressId).FirstOrDefault();
                if (address == null)
                    throw new Exception("Address can't be loaded");

                _workContext.CurrentCustomer.BillingAddress = address;
                if (address.CountryId.HasValue)
                    if (_countryService.GetCountryById(address.CountryId.Value).AllowsShipping)
                    {
                        _workContext.CurrentCustomer.ShippingAddress = address;
                        //_customerService.UpdateCustomer(_workContext.CurrentCustomer);
                    }




                var shippingAddressModel = PrepareShippingAddressModel(sameassBA: true, savebilling: true, idBillingAddress: address.Id);

                return Json(new
                    {
                        update_section_shippingAddress = new myUpdateSectionJsonModel()
                        {
                            name = "shipping",
                            html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel),
                        }
                    });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveOneBilling(FormCollection form)
        {
            try
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                int billingAddressId = 0;
                int.TryParse(form["NewAddress.Id"], out billingAddressId);

                if (billingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.Where(a => a.Id == billingAddressId).FirstOrDefault();
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    var model = new CheckoutBillingAddressModel();

                    TryUpdateModel(model.NewAddress, "BillingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);
                    ValidateZipPostalCode((model.NewAddress.CountryId.HasValue ? model.NewAddress.CountryId.Value : 0), (model.NewAddress.StateProvinceId.HasValue ? model.NewAddress.StateProvinceId.Value : 0), model.NewAddress.ZipPostalCode, true);

                    if (ModelState.IsValid)
                    {
                        int addressId = address.Id;
                        var country = address.Country;
                        var state = address.StateProvince;

                        address = model.NewAddress.ToEntity();
                        address.Id = addressId;
                        address.Country = country;
                        address.StateProvince = state;
                        address.CreatedOnUtc = DateTime.UtcNow;
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        //_addressService.UpdateAddress(address);
                        var context = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Data.IDbContext>();
                        var sql = "update Address set FirstName = {0}, LastName = {1}, Email = {2}, Company={3}, CountryId = {4}, StateProvinceId = {5}, City = {6}, Address1={7}, Address2={8}, ZipPostalCode={9}, PhoneNumber = {10}, FaxNumber = {11}  where Id = {12}";
                        context.ExecuteSqlCommand(sql, false, null, address.FirstName, address.LastName, address.Email, address.Company, address.CountryId, address.StateProvinceId, address.City, address.Address1, address.Address2, address.ZipPostalCode, address.PhoneNumber, address.FaxNumber, address.Id);

                        _workContext.CurrentCustomer.BillingAddress = address;
                        bool _ship = true;

                        if (address.CountryId.HasValue)
                        {
                            if (_countryService.GetCountryById(address.CountryId.Value).AllowsShipping)
                            {
                                _workContext.CurrentCustomer.ShippingAddress = address;
                            }
                            else
                            {
                                _ship = false;
                            }
                        }
                        _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                        //model is not valid. redisplay the form with errors
                        var billingAddressModel = PrepareBillingAddressModel(model.NewAddress.CountryId);
                        billingAddressModel.NewAddressPreselected = true;
                        billingAddressModel.NewAddress.Id = address.Id;
                        if (_ship)
                            return Json(new
                            {
                                update_section_billingAddress = new myUpdateSectionJsonModel()
                                {
                                    name = "billing",
                                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel)
                                },
                                oneok = true
                            });
                        else
                        {
                            var shippingAddressModel = PrepareShippingAddressModel();
                            shippingAddressModel.CanCheck = false;
                            shippingAddressModel.SameAsBillingAddress = false;
                            return Json(new
                            {
                                update_section_billingAddress = new myUpdateSectionJsonModel()
                                {
                                    name = "billing",
                                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel)
                                },
                                update_section_shippingAddress = new myUpdateSectionJsonModel()
                                {
                                    name = "shipping",
                                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel),
                                },
                                oneok = true
                            });
                        }
                    }
                    else
                    {
                        var billingAddressModel = PrepareBillingAddressModel(model.NewAddress.CountryId);
                        billingAddressModel.NewAddressPreselected = true;
                        billingAddressModel.NewAddress.Id = address.Id;
                        return Json(new
                        {
                            update_section_billingAddress = new myUpdateSectionJsonModel()
                            {
                                //saveAddress = true,
                                name = "billing",
                                html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel)
                            },
                            onerror = true
                        });
                    }


                }
                else
                {
                    var model = new CheckoutBillingAddressModel();

                    TryUpdateModel(model.NewAddress, "BillingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);

                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId, "");
                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //some validation
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        if (!(_addressSettings.CountryEnabled))
                        {
                            address.CountryId = null;
                            address.StateProvinceId = null;
                        }

                        TryUpdateModel(model.NewAddress, "BillingNewAddress");
                        //validate model
                        TryValidateModel(model.NewAddress);
                        ValidateZipPostalCode((model.NewAddress.CountryId.HasValue ? model.NewAddress.CountryId.Value : 0), (model.NewAddress.StateProvinceId.HasValue ? model.NewAddress.StateProvinceId.Value : 0), model.NewAddress.ZipPostalCode, true);

                        if (!ModelState.IsValid)
                        {
                            var billingAddressModel = PrepareBillingAddressModel(model.NewAddress.CountryId);
                            billingAddressModel.NewAddressPreselected = true;
                            return Json(new
                            {
                                update_section_billingAddress = new myUpdateSectionJsonModel()
                                {
                                    name = "billing",
                                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel)
                                },
                                onerror = true
                            });

                        }
                        else
                        {
                            _workContext.CurrentCustomer.Addresses.Add(address);
                            _workContext.CurrentCustomer.BillingAddress = address;

                            bool _ship = true;
                            if (address.CountryId.HasValue)
                                if (_countryService.GetCountryById(address.CountryId.Value).AllowsShipping)
                                {
                                    _workContext.CurrentCustomer.ShippingAddress = address;
                                }
                                else
                                {
                                    _ship = false;
                                }

                            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                            var billingAddressModel = PrepareBillingAddressModel(model.NewAddress.CountryId);
                            billingAddressModel.NewAddress.Id = address.Id;
                            billingAddressModel.NewAddressPreselected = true;
                            if (_ship)
                            {
                                return Json(new
                                {
                                    update_section_billingAddress = new myUpdateSectionJsonModel()
                                    {
                                        name = "billing",
                                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel)
                                    },
                                    oneok = true
                                });
                            }
                            else
                            {
                                var shippingAddressModel = PrepareShippingAddressModel();
                                shippingAddressModel.CanCheck = false;
                                shippingAddressModel.SameAsBillingAddress = false;
                                return Json(new
                                {
                                    update_section_billingAddress = new myUpdateSectionJsonModel()
                                    {
                                        name = "billing",
                                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel)
                                    },
                                    update_section_shippingAddress = new myUpdateSectionJsonModel()
                                    {
                                        name = "shipping",
                                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel),
                                    },
                                    oneok = true
                                });
                            }
                        }

                    }
                    else
                    {


                        TryUpdateModel(model.NewAddress, "BillingNewAddress");
                        //validate model
                        TryValidateModel(model.NewAddress);
                        ValidateZipPostalCode((model.NewAddress.CountryId.HasValue ? model.NewAddress.CountryId.Value : 0), (model.NewAddress.StateProvinceId.HasValue ? model.NewAddress.StateProvinceId.Value : 0), model.NewAddress.ZipPostalCode, true);

                        if (!ModelState.IsValid)
                        {
                            var billingAddressModel = PrepareBillingAddressModel(model.NewAddress.CountryId);
                            billingAddressModel.NewAddressPreselected = true;
                            billingAddressModel.NewAddress.Id = address.Id;
                            return Json(new
                            {
                                update_section_billingAddress = new myUpdateSectionJsonModel()
                                {
                                    name = "billing",
                                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel)
                                },
                                oneerror = true
                            });
                            //return Json(new { error = 1, message = "Please set field" });
                        }
                        else
                        {
                            _workContext.CurrentCustomer.Addresses.Add(address);
                            _workContext.CurrentCustomer.BillingAddress = address;

                            bool _ship = true;
                            if (address.CountryId.HasValue)
                            {
                                if (_countryService.GetCountryById(address.CountryId.Value).AllowsShipping)
                                {
                                    _workContext.CurrentCustomer.ShippingAddress = address;
                                }
                                else
                                {
                                    _ship = false;
                                }
                            }
                            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                            var billingAddressModel = PrepareBillingAddressModel(model.NewAddress.CountryId);
                            billingAddressModel.NewAddress.Id = address.Id;
                            billingAddressModel.NewAddressPreselected = true;
                            if (_ship)
                            {
                                return Json(new
                                {
                                    update_section_billingAddress = new myUpdateSectionJsonModel()
                                    {
                                        name = "billing",
                                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel)
                                    },
                                    oneok = true
                                });
                            }
                            else
                            {
                                var shippingAddressModel = PrepareShippingAddressModel();
                                shippingAddressModel.CanCheck = false;
                                shippingAddressModel.SameAsBillingAddress = false;
                                return Json(new
                                {
                                    update_section_billingAddress = new myUpdateSectionJsonModel()
                                    {
                                        name = "billing",
                                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel)
                                    },
                                    update_section_shippingAddress = new myUpdateSectionJsonModel()
                                    {
                                        name = "shipping",
                                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel),
                                    },
                                    oneok = true
                                });
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                return Json(new { error = 1, message = ex.Message });
            }
        }


        [ValidateInput(false)]
        public ActionResult OpcSaveOneShipping(FormCollection form)
        {
            try
            {
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                int shippingAddressId = 0;
                int.TryParse(form["NewAddress.Id"], out shippingAddressId);

                if (shippingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.Where(a => a.Id == shippingAddressId).FirstOrDefault();
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    var model = new CheckoutShippingAddressModel();

                    TryUpdateModel(model.NewAddress, "ShippingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);

                    // validation ZIP
                    ValidateZipPostalCode((model.NewAddress.CountryId.HasValue ? model.NewAddress.CountryId.Value : 0), (model.NewAddress.StateProvinceId.HasValue ? model.NewAddress.StateProvinceId.Value : 0), model.NewAddress.ZipPostalCode, false);


                    if (ModelState.IsValid)
                    {
                        int addressId = address.Id;
                        var country = address.Country;
                        var state = address.StateProvince;

                        address = model.NewAddress.ToEntity();
                        address.Id = addressId;
                        address.Country = country;
                        address.StateProvince = state;
                        address.CreatedOnUtc = DateTime.UtcNow;
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        //_addressService.UpdateAddress(address);
                        var context = Nop.Core.Infrastructure.EngineContext.Current.Resolve<Nop.Data.IDbContext>();
                        var sql = "update Address set FirstName = {0}, LastName = {1}, Email = {2}, Company={3}, CountryId = {4}, StateProvinceId = {5}, City = {6}, Address1={7}, Address2={8}, ZipPostalCode={9}, PhoneNumber = {10}, FaxNumber = {11}  where Id = {12}";
                        context.ExecuteSqlCommand(sql, false, null, address.FirstName, address.LastName, address.Email, address.Company, address.CountryId, address.StateProvinceId, address.City, address.Address1, address.Address2, address.ZipPostalCode, address.PhoneNumber, address.FaxNumber, address.Id);

                        _workContext.CurrentCustomer.ShippingAddress = address;
                        _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                        //model is not valid. redisplay the form with errors
                        var shippingAddressModel = PrepareShippingAddressModel();
                        shippingAddressModel.CanCheck = false;
                        shippingAddressModel.SameAsBillingAddress = false;
                        return Json(new
                        {
                            update_section_shippingAddress = new myUpdateSectionJsonModel()
                            {
                                name = "shipping",
                                html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel),
                            },
                            oneok = true
                        });
                    }
                    else
                    {
                        var shippingAddressModel = PrepareShippingAddressModel(model.NewAddress.CountryId);
                        shippingAddressModel.NewAddressPreselected = true;
                        shippingAddressModel.NewAddress.Id = address.Id;
                        shippingAddressModel.SameAsBillingAddress = false;

                        return Json(new
                        {
                            update_section_shippingAddress = new myUpdateSectionJsonModel()
                            {
                                //saveAddress = true,
                                name = "shipping",
                                html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel)
                            }
                        });
                    }


                }
                else
                {
                    var model = new CheckoutShippingAddressModel();

                    TryUpdateModel(model.NewAddress, "ShippingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);
                    // validation ZIP
                    ValidateZipPostalCode((model.NewAddress.CountryId.HasValue ? model.NewAddress.CountryId.Value : 0), (model.NewAddress.StateProvinceId.HasValue ? model.NewAddress.StateProvinceId.Value : 0), model.NewAddress.ZipPostalCode, false);

                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId, "");
                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //some validation
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;

                        TryUpdateModel(model.NewAddress, "ShippingNewAddress");
                        //validate model
                        TryValidateModel(model.NewAddress);
                        // validation ZIP
                        ValidateZipPostalCode((model.NewAddress.CountryId.HasValue ? model.NewAddress.CountryId.Value : 0), (model.NewAddress.StateProvinceId.HasValue ? model.NewAddress.StateProvinceId.Value : 0), model.NewAddress.ZipPostalCode, false);

                        if (!ModelState.IsValid)
                        {
                            var shippingAddressModel = PrepareShippingAddressModel(model.NewAddress.CountryId);
                            shippingAddressModel.NewAddressPreselected = true;
                            shippingAddressModel.SameAsBillingAddress = false;

                            return Json(new
                            {
                                update_section_shippingAddress = new myUpdateSectionJsonModel()
                                {
                                    name = "shipping",
                                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel)
                                }
                            });

                        }
                        else
                        {
                            _workContext.CurrentCustomer.Addresses.Add(address);
                            _workContext.CurrentCustomer.ShippingAddress = address;
                            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                            var shippingAddressModel = PrepareShippingAddressModel(model.NewAddress.CountryId);
                            shippingAddressModel.NewAddress.Id = address.Id;
                            shippingAddressModel.NewAddressPreselected = true;
                            shippingAddressModel.SameAsBillingAddress = false;

                            return Json(new
                            {
                                update_section_shippingAddress = new myUpdateSectionJsonModel()
                                {
                                    name = "shipping",
                                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel)
                                },
                                oneok = true
                            });
                        }

                    }
                    else
                    {


                        TryUpdateModel(model.NewAddress, "ShippingNewAddress");
                        //validate model
                        TryValidateModel(model.NewAddress);
                        // validation ZIP
                        ValidateZipPostalCode((model.NewAddress.CountryId.HasValue ? model.NewAddress.CountryId.Value : 0), (model.NewAddress.StateProvinceId.HasValue ? model.NewAddress.StateProvinceId.Value : 0), model.NewAddress.ZipPostalCode, false);

                        if (!ModelState.IsValid)
                        {
                            var shippingAddressModel = PrepareShippingAddressModel(model.NewAddress.CountryId);
                            shippingAddressModel.NewAddressPreselected = true;
                            shippingAddressModel.NewAddress.Id = address.Id;
                            shippingAddressModel.SameAsBillingAddress = false;

                            return Json(new
                            {
                                update_section_shippingAddress = new myUpdateSectionJsonModel()
                                {
                                    name = "shipping",
                                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel)
                                }
                            });
                            //return Json(new { error = 1, message = "Please set field" });
                        }
                        else
                        {
                            _workContext.CurrentCustomer.Addresses.Add(address);
                            _workContext.CurrentCustomer.ShippingAddress = address;
                            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                            var shippingAddressModel = PrepareShippingAddressModel(model.NewAddress.CountryId);
                            shippingAddressModel.SameAsBillingAddress = false;
                            shippingAddressModel.NewAddress.Id = address.Id;
                            shippingAddressModel.NewAddressPreselected = true;
                            return Json(new
                            {
                                update_section_shippingAddress = new myUpdateSectionJsonModel()
                                {
                                    name = "shipping",
                                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel)
                                },
                                oneok = true
                            });
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                return Json(new { error = 1, message = ex.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveBilling(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                int billingAddressId = 0;
                int.TryParse(form["billing_address_id"], out billingAddressId);

                SingleCheckoutShippingAddressModel scsam = new SingleCheckoutShippingAddressModel();
                scsam.idBillingAddress = 0;

                if (billingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.Where(a => a.Id == billingAddressId).FirstOrDefault();
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.BillingAddress = address;
                    if (_workContext.CurrentCustomer.Addresses.Count == 1)
                        _workContext.CurrentCustomer.ShippingAddress = address;

                    if (address.CountryId.HasValue)
                        if (_countryService.GetCountryById(address.CountryId.Value).AllowsShipping)
                        {
                            _workContext.CurrentCustomer.ShippingAddress = address;
                        }
                    scsam.idBillingAddress = address.Id;
                    scsam = PrepareShippingAddressModel(sameassBA: true, savebilling: true, idBillingAddress: address.Id);


                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var model = new CheckoutBillingAddressModel();
                    TryUpdateModel(model.NewAddress, "BillingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);
                    //custom address attributes
                    var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
                    var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
                    foreach (var error in customAttributeWarnings)
                    {
                        ModelState.AddModelError("", error);
                    }

                    // validation ZIP
                    ValidateZipPostalCode((model.NewAddress.CountryId.HasValue ? model.NewAddress.CountryId.Value : 0), (model.NewAddress.StateProvinceId.HasValue ? model.NewAddress.StateProvinceId.Value : 0), model.NewAddress.ZipPostalCode, true);

                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var billingAddressModel = PrepareBillingAddressModel(model.NewAddress.CountryId);
                        billingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section_billingAddress = new myUpdateSectionJsonModel()
                            {
                                //saveAddress = true,
                                name = "billing",
                                html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel)
                            }
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId, "");
                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //some validation
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        _workContext.CurrentCustomer.Addresses.Add(address);
                    }

                    _workContext.CurrentCustomer.BillingAddress = address;
                    if (address.CountryId.HasValue)
                        if (_countryService.GetCountryById(address.CountryId.Value).AllowsShipping)
                        {
                            _workContext.CurrentCustomer.ShippingAddress = address;
                        }

                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                    if (address.CountryId.HasValue)
                        if (_countryService.GetCountryById(address.CountryId.Value).AllowsShipping)
                        {
                            scsam.idBillingAddress = address.Id;
                            scsam = PrepareShippingAddressModel(sameassBA: true, savebilling: true, idBillingAddress: address.Id);
                        }

                }

                var billingAddressModel2 = PrepareBillingAddressModel();
                var shippingAddressModel = (scsam.idBillingAddress > 0 ? scsam : PrepareShippingAddressModel(sameassBA: true, savebilling: true));
                if (_workContext.CurrentCustomer.BillingAddress != null)
                    if (_workContext.CurrentCustomer.BillingAddress.Country != null)
                        if (!_workContext.CurrentCustomer.BillingAddress.Country.AllowsShipping)
                        {
                            shippingAddressModel.SameAsBillingAddress = false;
                        }

                var shippingMethodModel = PrepareShippingMethodModel(cart);
                var confirmOrderModel = PrepareConfirmOrderModel(cart);
                return Json(new
                {
                    update_section_shippingAddress = new myUpdateSectionJsonModel()
                    {
                        saveAddress = true,
                        name = "shipping",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel),
                    }
                    ,
                    update_section_billingAddress = new myUpdateSectionJsonModel()
                    {
                        name = "billing",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcBillingAddress.cshtml", billingAddressModel2)
                    },
                    update_section_selectedBillingAddress = new myUpdateSectionJsonModel()
                    {
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/SelectedBillingAddress.cshtml")
                    }
                    ,
                    update_section_shipping = new myUpdateSectionJsonModel()
                    {
                        name = "shipping-method",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingMethods.cshtml", shippingMethodModel)
                    },
                    update_section_order = new myUpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcConfirmOrder.cshtml", confirmOrderModel)
                    }

                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSaveShipping(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                int shippingAddressId = 0;
                int.TryParse(form["shipping_address_id"], out shippingAddressId);

                if (shippingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.Where(a => a.Id == shippingAddressId).LastOrDefault();
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var model = new SingleCheckoutShippingAddressModel();
                    TryUpdateModel(model.NewAddress, "ShippingNewAddress");
                    //validate model
                    string sameAsBA = form["SameAsBillingAddress"];
                    model.SameAsBillingAddress = sameAsBA == "True" ? true : false;
                    TryValidateModel(model.NewAddress);
                    // validation ZIP
                    ValidateZipPostalCode((model.NewAddress.CountryId.HasValue ? model.NewAddress.CountryId.Value : 0), (model.NewAddress.StateProvinceId.HasValue ? model.NewAddress.StateProvinceId.Value : 0), model.NewAddress.ZipPostalCode, false);

                    //custom address attributes
                    var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
                    var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
                    foreach (var error in customAttributeWarnings)
                    {
                        ModelState.AddModelError("", error);
                    }


                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var shippingAddressModel = PrepareShippingAddressModel(model.NewAddress.CountryId, model.SameAsBillingAddress);
                        shippingAddressModel.NewAddressPreselected = true;

                        return Json(new
                        {
                            update_section_shippingAddress = new myUpdateSectionJsonModel()
                            {
                                saveAddress = false,
                                name = "shipping",
                                html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel)
                            }
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                        model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                        model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                        model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                        model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId, "");
                    if (address == null)
                    {
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //little hack here (TODO: find a better solution)
                        //EF does not load navigation properties for newly created entities (such as this "Address").
                        //we have to load them manually 
                        //otherwise, "Country" property of "Address" entity will be null in shipping rate computation methods
                        if (address.CountryId.HasValue)
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        if (address.StateProvinceId.HasValue)
                            address.StateProvince = _stateProvinceService.GetStateProvinceById(address.StateProvinceId.Value);

                        //other null validations
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        _workContext.CurrentCustomer.Addresses.Add(address);
                    }
                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }

                string sameAsBA1 = form["SameAsBillingAddress"];
                //model.SameAsBillingAddress = sameAsBA == "True" ? true : false;

                bool sameasBA = sameAsBA1 == "True" ? true : false; //Convert.ToBoolean(form["SameAsBillingAddress"]);


                var shippingAddressModel2 = PrepareShippingAddressModel(sameassBA: sameasBA);
                var shippingMethodModel = PrepareShippingMethodModel(cart);

                var confirmOrderModel = PrepareConfirmOrderModel(cart);

                return Json(new
                {
                    update_section_shippingAddress = new myUpdateSectionJsonModel()
                    {
                        saveAddress = true,
                        name = "shipping",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingAddress.cshtml", shippingAddressModel2)
                    },
                    update_section_shipping = new myUpdateSectionJsonModel()
                    {
                        name = "shipping-method",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingMethods.cshtml", shippingMethodModel)
                    },
                    update_section_order = new myUpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcConfirmOrder.cshtml", confirmOrderModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }


        [ValidateInput(false)]
        public ActionResult OpcChangeShippingAddress(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                int shippingAddressId = 0;
                int.TryParse(form["shipping_address_id"], out shippingAddressId);

                if (shippingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.Where(a => a.Id == shippingAddressId).LastOrDefault();
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.ShippingAddress = address;
                    //_customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                var shippingMethodModel = PrepareShippingMethodModel(cart);

                return Json(new
                {
                    update_section_shipping = new myUpdateSectionJsonModel()
                    {
                        name = "shipping-method",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcShippingMethods.cshtml", shippingMethodModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }


        [ValidateInput(false)]
        public ActionResult OpcSaveShippingMethod(string shippingmethod)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                //parse selected method 
                string shippingoption = shippingmethod; //form["shippingoption"];
                if (String.IsNullOrEmpty(shippingoption))
                    throw new Exception("Selected shipping method can't be parsed");
                var splittedOption = shippingoption.Split(new string[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
                if (splittedOption.Length != 2)
                    throw new Exception("Selected shipping method can't be parsed");
                string selectedName = splittedOption[0];
                string shippingRateComputationMethodSystemName = splittedOption[1];

                //find it
                //performance optimization. try cache first
                var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions);
                if (shippingOptions == null || shippingOptions.Count == 0)
                {
                    //not found? let's load them using shipping service
                    var model2 = new CheckoutShippingMethodModel();
                    var shippingOptionToSelect = model2.ShippingMethods.FirstOrDefault();
                    if (shippingOptionToSelect != null)
                    {
                        shippingOptionToSelect.Selected = true;
                    }
                     
                }
                else
                {
                    //loaded cached results. let's filter result by a chosen shipping rate computation method
                    shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                        .ToList();
                }

                var shippingOption = shippingOptions
                    .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
                if (shippingOption == null)
                    throw new Exception("Selected shipping method can't be loaded");

                //save
                //_genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.Shi, shippingOption);
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);

                //var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst);
                int filterByCountryId = 0;
                if (_addressSettings.CountryEnabled &&
                    _workContext.CurrentCustomer.BillingAddress != null &&
                    _workContext.CurrentCustomer.BillingAddress.Country != null)
                {
                    filterByCountryId = _workContext.CurrentCustomer.BillingAddress.Country.Id;
                }
                var paymentMethodModel = PreparePaymentMethodModel(cart, filterByCountryId);

                var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName);
                var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.SelectedPaymentMethod, _genericAttributeService, _storeContext.CurrentStore.Id);
                if (!String.IsNullOrEmpty(selectedPaymentMethodSystemName))
                {
                    paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
                }
                var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst);


                var confirmOrderModel = PrepareConfirmOrderModel(cart);
                return Json(new
                {
                    update_section_order = new myUpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcConfirmOrder.cshtml", confirmOrderModel)
                    },
                    update_section_paymentInfo = new myUpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcPaymentInfo.cshtml", paymenInfoModel)
                    },
                });

            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSavePaymentMethod(string payment, bool userewardpoints)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                   .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                   .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                   .ToList();

                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                string paymentmethod = payment; //form["paymentmethod"];
                //payment method 
                if (String.IsNullOrEmpty(paymentmethod))
                    throw new Exception("Selected payment method can't be parsed");


                var model = new CheckoutPaymentMethodModel();
                TryUpdateModel(model);

                //reward points
                if (_rewardPointsSettings.Enabled)
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, userewardpoints,
                        _storeContext.CurrentStore.Id);
                }

                var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
                if (paymentMethodInst == null || !paymentMethodInst.IsPaymentMethodActive(_paymentSettings))
                    throw new Exception("Selected payment method can't be parsed");

                //save
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, _storeContext.CurrentStore.Id);

                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                var paymenInfoModel = PreparePaymentInfoModel(paymentMethodInst);
                var confirmOrderModel = PrepareConfirmOrderModel(cart);

                return Json(new
                {
                    update_section = new myUpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcPaymentInfo.cshtml", paymenInfoModel)
                    },
                    update_section_order = new myUpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcConfirmOrder.cshtml", confirmOrderModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcSavePaymentInfo(FormCollection form)
        {
            try
            {

                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                                    .ToList();

                if (cart.Count == 0)
                    throw new NopException("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new NopException("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new NopException("Anonymous checkout is not allowed");

                var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);

                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
                if (paymentMethod == null)
                    throw new NopException("Payment method is not selected");

                var paymentControllerType = paymentMethod.GetControllerType();
                var paymentController =
                    DependencyResolver.Current.GetService(paymentControllerType) as BasePaymentController;
                var warnings = paymentController.ValidatePaymentForm(form);
                string strWarnings = "";
                foreach (var warning in warnings)
                {
                    ModelState.AddModelError("", warning);
                    strWarnings = strWarnings + warning + " \n";
                }
                if (ModelState.IsValid)
                {

                    //get payment info
                    var paymentInfo = paymentController.GetPaymentInfo(form);
                    //session save
                    _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);

                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcConfirmOrder.cshtml", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }


                var paymenInfoModel = PreparePaymentInfoModel(paymentMethod);
                return Json(new { error = 1, message = strWarnings });
            }
            catch (Exception exc)
            {
                _logger.Error(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        [ValidateInput(false)]
        public ActionResult OpcRefreshOrder(FormCollection form)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();

            _httpContext.Session["OrderPaymentInfo"] = "info";
            var confirmOrderModel = PrepareConfirmOrderModel(cart);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel()
                {
                    name = "confirm-order",
                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OpcConfirmOrder.cshtml", confirmOrderModel)
                },
                goto_section = "confirm_order"
            });
        }

        [ValidateInput(false)]
        public ActionResult OpcConfirmOrder(FormCollection form)
        {
            try
            {
                bool samassBilling = form["sameasbillingaddress"].Contains("true");
                if (samassBilling)
                {
                    _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                //validation                
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                if (!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                        SystemCustomerAttributeNames.SelectedPaymentMethod,
                        _genericAttributeService, _storeContext.CurrentStore.Id);


                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
                if (paymentMethod == null)
                    throw new NopException("Payment method is not selected");

                bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);

                var paymentControllerType = paymentMethod.GetControllerType();
                var paymentController =
                    DependencyResolver.Current.GetService(paymentControllerType) as BasePaymentController;
                var warnings = paymentController.ValidatePaymentForm(form);
                string strWarnings = "";
                foreach (var warning in warnings)
                {
                    ModelState.AddModelError("", warning);
                    strWarnings = strWarnings + warning + " \n";
                }
                if ((ModelState.IsValid) || (!isPaymentWorkflowRequired))
                {

                    //get payment info
                    var paymentInfo = paymentController.GetPaymentInfo(form);
                    //session save
                    _httpContext.Session["OrderPaymentInfo"] = paymentInfo;
                }
                else
                {
                    if (isPaymentWorkflowRequired)
                        return Json(new { error = 1, message = strWarnings });
                }

                //place order
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (IsPaymentWorkflowRequired(cart))
                    {
                        throw new Exception("Payment information is not entered");
                    }
                    else
                        processPaymentRequest = new ProcessPaymentRequest();
                }


                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = selectedPaymentMethodSystemName;
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                if (placeOrderResult.Success)
                {
                    _httpContext.Session["OrderPaymentInfo"] = null;
                    var postProcessPaymentRequest = new PostProcessPaymentRequest()
                    {
                        Order = placeOrderResult.PlacedOrder
                    };

                    if (paymentMethod != null)
                    {
                        if (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection)
                        {
                            return Json(new { redirect = string.Format("{0}checkout/OpcCompleteRedirectionPayment", _webHelper.GetStoreLocation()) });
                        }
                        else
                        {
                            _paymentService.PostProcessPayment(postProcessPaymentRequest);

                            return Json(new { success = 1 });
                        }
                    }
                    else
                    {
                        return Json(new { success = 1 });
                    }
                }
                else
                {
                    //error
                    string strError = "";
                    var confirmOrderModel = new CheckoutConfirmModel();
                    foreach (var error in placeOrderResult.Errors)
                    {
                        strError = strError + error + " \n";
                        confirmOrderModel.Warnings.Add(error);
                    }
                    throw new Exception(strError);
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new { error = 1, message = exc.Message });
            }
        }

        public ActionResult OpcCompleteRedirectionPayment()
        {
            try
            {
                //validation
                if (!UseOnePageCheckout())
                    return RedirectToRoute("HomePage");

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    return new HttpUnauthorizedResult();

                //get the order
                var order = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id, customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                    .FirstOrDefault();
                if (order == null)
                    return RedirectToRoute("HomePage");


                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                if (paymentMethod == null)
                    return RedirectToRoute("HomePage");
                if (paymentMethod.PaymentMethodType != PaymentMethodType.Redirection)
                    return RedirectToRoute("HomePage");

                //ensure that order has been just placed
                if ((DateTime.UtcNow - order.CreatedOnUtc).TotalMinutes > 3)
                    return RedirectToRoute("HomePage");


                //Redirection will not work on one page checkout page because it's AJAX request.
                //That's why we process it here
                var postProcessPaymentRequest = new PostProcessPaymentRequest()
                {
                    Order = order
                };

                _paymentService.PostProcessPayment(postProcessPaymentRequest);

                if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                {
                    //redirection or POST has been done in PostProcessPayment
                    return Content("Redirected");
                }
                else
                {
                    //if no redirection has been done (to a third-party payment page)
                    //theoretically it's not possible
                    return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Content(exc.Message);
            }
        }


        [ChildActionOnly]
        public ActionResult OrderSummary(bool? prepareAndDisplayOrderReviewData)
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();
            PrepareShoppingCartModel(model, cart,
                isEditable: true,
                prepareEstimateShippingIfEnabled: false,
                prepareAndDisplayOrderReviewData: prepareAndDisplayOrderReviewData.GetValueOrDefault());
            //let's apply gift cards now (gift cards that can be used)
            var appliedGiftCards = new List<AppliedGiftCard>();
            if (!cart.IsRecurring())
            {
                //we don't apply gift cards for recurring products
                var giftCards = _giftCardService.GetActiveGiftCardsAppliedByCustomer(_workContext.CurrentCustomer);
                if (giftCards != null)
                    foreach (var gc in giftCards)
                    {
                        model.GiftCardBox.CustomProperties.Add(gc.GiftCardCouponCode, null);
                    }

            }
            return PartialView("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OrderSummary.cshtml", model);
        }

        [ValidateInput(false)]
        [HttpPost, ActionName("onepagecheckout")]
        [FormValueRequired("updatecart")]
        public ActionResult UpdateCart(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart))
                return RedirectToRoute("HomePage");

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            var allIdsToRemove = form["removefromcart"] != null ? form["removefromcart"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();

            //current warnings <cart item identifier, warnings>
            var innerWarnings = new Dictionary<int, IList<string>>();
            foreach (var sci in cart)
            {
                bool remove = allIdsToRemove.Contains(sci.Id);
                if (remove)
                    _shoppingCartService.DeleteShoppingCartItem(sci, ensureOnlyActiveCheckoutAttributes: true);
                else
                {
                    foreach (string formKey in form.AllKeys)
                        if (formKey.Equals(string.Format("itemquantity{0}", sci.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            int newQuantity;
                            if (int.TryParse(form[formKey], out newQuantity))
                            {
                                var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(_workContext.CurrentCustomer,
                                    sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice,
                                    sci.RentalStartDateUtc, sci.RentalEndDateUtc,
                                    newQuantity, true);
                                innerWarnings.Add(sci.Id, currSciWarnings);
                            }
                            break;
                        }
                }
            }

            return OnePageCheckout();
        }



        [ValidateInput(false)]        
        public ActionResult ApplyDiscountCoupon(string discountcouponcode)
        {
            //trim
            if (discountcouponcode != null)
                discountcouponcode = discountcouponcode.Trim();

            //cart
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();

            var model = new ShoppingCartModel();
            if (!String.IsNullOrWhiteSpace(discountcouponcode))
            {
                //we find even hidden records here. this way we can display a user-friendly message if it's expired
                var discounts = _discountService.GetAllDiscountsForCaching(couponCode: discountcouponcode, showHidden: true)
                  .Where(d => d.RequiresCouponCode)
                  .ToList();

                if (discounts.Any())
                {
                    var userErrors = new List<string>();
                    var anyValidDiscount = discounts.Any(discount =>
                    {
                        var validationResult = _discountService.ValidateDiscount(discount, _workContext.CurrentCustomer, new[] { discountcouponcode });
                        userErrors.AddRange(validationResult.Errors);

                        return validationResult.IsValid;
                    });

                    if (anyValidDiscount)
                    {
                        //valid
                        _workContext.CurrentCustomer.ApplyDiscountCouponCode(discountcouponcode);
                        model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.Applied"));
                        model.DiscountBox.IsApplied = true;
                    }
                    else
                    {
                        if (userErrors.Any())
                            //some user errors
                            model.DiscountBox.Messages = userErrors;
                        else
                            //general error text
                            model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
                    }
                }
                else
                    model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
            }
            else
            {   //empty coupon code
                model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
            }

            PrepareShoppingCartModel(model, cart);
            if(!model.DiscountBox.IsApplied)
            {
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "coupon-box",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/_DiscountBox.cshtml", model.DiscountBox)
                    },
                });
            }

            var totalOrderModel = PrepareOrderTotalsModel(cart, false);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel()
                {
                    name = "coupon-box",
                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/_DiscountBox.cshtml", model.DiscountBox)
                },
                update_section_totals = new UpdateSectionJsonModel()
                {
                    name = "totals",
                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OrderTotals.cshtml", totalOrderModel)
                },
            });

        }
        [ValidateInput(false)]
        public ActionResult RemoveDiscountCoupon()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            var model = new ShoppingCartModel();

            _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.DiscountCouponCode, null);


            PrepareShoppingCartModel(model, cart);

            var totalOrderModel = PrepareOrderTotalsModel(cart, false);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel()
                {
                    name = "coupon-box",
                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/_DiscountBox.cshtml", model.DiscountBox)
                },
                update_section_totals = new UpdateSectionJsonModel()
                {
                    name = "totals",
                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OrderTotals.cshtml", totalOrderModel)
                },
            });

        }


        [ValidateInput(false)]
        public ActionResult ApplyGiftCard(string giftcardcouponcode)
        {
            //trim
            if (giftcardcouponcode != null)
                giftcardcouponcode = giftcardcouponcode.Trim();

            //cart
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
           
            var model = new ShoppingCartModel();
            if (!cart.IsRecurring())
            {
                if (!String.IsNullOrWhiteSpace(giftcardcouponcode))
                {
                    var giftCard = _giftCardService.GetAllGiftCards(giftCardCouponCode: giftcardcouponcode).FirstOrDefault();
                    bool isGiftCardValid = giftCard != null && giftCard.IsGiftCardValid();
                    if (isGiftCardValid)
                    {
                        _workContext.CurrentCustomer.ApplyGiftCardCouponCode(giftcardcouponcode);
                        _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                        model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.Applied");
                        model.GiftCardBox.IsApplied = true;
                    }
                    else
                    {
                        model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.WrongGiftCard");
                        model.GiftCardBox.IsApplied = false;
                    }
                }
                else
                {
                    model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.WrongGiftCard");
                    model.GiftCardBox.IsApplied = false;
                }
            }
            else
            {
                model.GiftCardBox.Message = _localizationService.GetResource("ShoppingCart.GiftCardCouponCode.DontWorkWithAutoshipProducts");
                model.GiftCardBox.IsApplied = false;
            }

            PrepareShoppingCartModel(model, cart);
            //let's apply gift cards now (gift cards that can be used)
            var appliedGiftCards = new List<AppliedGiftCard>();
            if (!cart.IsRecurring())
            {
                //we don't apply gift cards for recurring products
                var giftCards = _giftCardService.GetActiveGiftCardsAppliedByCustomer(_workContext.CurrentCustomer);
                if (giftCards != null)
                    foreach (var gc in giftCards)
                    {
                        model.GiftCardBox.CustomProperties.Add(gc.GiftCardCouponCode, null);
                    }

            }

            if (!model.GiftCardBox.IsApplied)
            {
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "giftcard-box",
                        html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/_GiftCardBox.cshtml", model.GiftCardBox)
                    },
                });

            }

            var totalOrderModel = PrepareOrderTotalsModel(cart, false);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel()
                {
                    name = "giftcard-box",
                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/_GiftCardBox.cshtml", model.GiftCardBox)
                },
                update_section_totals = new UpdateSectionJsonModel()
                {
                    name = "totals",
                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OrderTotals.cshtml", totalOrderModel)
                },
            });
        }

        [ValidateInput(false)]
        public ActionResult RemoveGiftCardCode(string giftcard)
        {
            var model = new ShoppingCartModel();

            _workContext.CurrentCustomer.RemoveGiftCardCouponCode(giftcard);
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id)
                .ToList();
            PrepareShoppingCartModel(model, cart);

            //let's apply gift cards now (gift cards that can be used)
            var appliedGiftCards = new List<AppliedGiftCard>();
            if (!cart.IsRecurring())
            {
                //we don't apply gift cards for recurring products
                var giftCards = _giftCardService.GetActiveGiftCardsAppliedByCustomer(_workContext.CurrentCustomer);
                if (giftCards != null)
                    foreach (var gc in giftCards)
                    {
                        model.GiftCardBox.CustomProperties.Add(gc.GiftCardCouponCode, null);
                    }

            }

            var totalOrderModel = PrepareOrderTotalsModel(cart, false);
            return Json(new
            {
                update_section = new UpdateSectionJsonModel()
                {
                    name = "giftcard-box",
                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/_GiftCardBox.cshtml", model.GiftCardBox)
                },
                update_section_totals = new UpdateSectionJsonModel()
                {
                    name = "totals",
                    html = this.RenderPartialViewToString("~/Plugins/Misc.SingleCheckout/Views/MiscCheckout/Checkout/OrderTotals.cshtml", totalOrderModel)
                },
            });            
        }


        #endregion
    }
}
