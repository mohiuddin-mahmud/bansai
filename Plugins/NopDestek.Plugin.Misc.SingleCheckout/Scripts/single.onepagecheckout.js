

var myAddressBilling = false;
var myAddressShipping = false;
var mySaveAddressBilling = false;
var mySaveAddressShipping = false;


var Checkout = {
    loadWaiting: false,
    failureUrl: false,
    steps: new Array(),

    init: function (failureUrl) {
        this.loadWaiting = false;
        this.failureUrl = failureUrl;
        this.steps = ['billing', 'shipping', 'shipping_method', 'payment_method', 'payment_info', 'confirm_order'];

        Accordion.disallowAccessToNextSections = true;
    },

    ajaxFailure: function () {
        location.href = Checkout.failureUrl;
    },

    _disableEnableAll: function (element, isDisabled) {
        var descendants = element.find('*');
        $(descendants).each(function () {
            if (isDisabled) {
                $(this).attr('disabled', 'disabled');
            } else {
                $(this).removeAttr('disabled');
            }
        });

        if (isDisabled) {
            element.attr('disabled', 'disabled');
        } else {
            element.removeAttr('disabled');
        }
    },

    setLoadWaiting: function (step, keepDisabled) {
        if (step) {
            if (this.loadWaiting) {
                this.setLoadWaiting(false);
            }
            var container = $('#' + step + '-buttons-container');
            container.addClass('disabled');
            container.css('opacity', '.5');
            this._disableEnableAll(container, true);
            $('#' + step + '-please-wait').show();
        } else {
            if (this.loadWaiting) {
                var container = $('#' + this.loadWaiting + '-buttons-container');
                var isDisabled = (keepDisabled ? true : false);
                if (!isDisabled) {
                    container.removeClass('disabled');
                    container.css('opacity', '1');
                }
                this._disableEnableAll(container, isDisabled);
                $('#' + this.loadWaiting + '-please-wait').hide();
            }
        }
        this.loadWaiting = step;
    },

    gotoSection: function (section) {
        section = $('#opc-' + section);
        section.addClass('allow');
        Accordion.openSection(section);
    },

    back: function () {
        if (this.loadWaiting) return;
        Accordion.openPrevSection(true, true);
    },

    setStepResponse: function (response) {
        if (response.update_section) {
            $('#checkout-' + response.update_section.name + '-load').html(response.update_section.html);
        }
        if (response.update_section_order) {
            $('#checkout-' + response.update_section_order.name + '-load').html(response.update_section_order.html);
        }
        if (response.update_section_billingAddress) {
            $('#checkout-' + response.update_section_billingAddress.name + '-load').html(response.update_section_billingAddress.html);
        }
        if (response.update_section_shipping) {
            $('#checkout-' + response.update_section_shipping.name + '-load').html(response.update_section_shipping.html);
        }
        if (response.allow_sections) {
            response.allow_sections.each(function (e) {
                $('#opc-' + e).addClass('allow');
            });
        }

        //TODO move it to a new method
        if ($("#billing-address-select").length > 0) {
            Billing.newAddress(!$('#billing-address-select').val());
        }
        if ($("#shipping-address-select").length > 0) {
            Shipping.newAddress(!$('#shipping-address-select').val());
        }

        if (response.goto_section) {
            Checkout.gotoSection(response.goto_section);
            return true;
        }
        if (response.redirect) {
            location.href = response.redirect;
            return true;
        }
        return false;
    }
};





var Billing = {
    form: false,
    saveUrl: false,
    saveOneUrl: false,

    init: function (form, saveUrl, saveoneUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
        this.saveOneUrl = saveoneUrl;
    },

    newAddress: function (isNew) {
        if (isNew) {
            myAddressBilling = false;
            this.resetSelectedAddress();
            $('#BillingselectedAddress').hide();
            $('#billing-new-address-form').show();
        } else {
            $('#billing-new-address-form').hide();
            $('#BillingselectedAddress').show();

            if (mySaveAddressBilling == false)
                this.saveOrderInfo();

            mySaveAddressBilling = false;
            myAddressBilling = true;
            mySaveAddressShipping = true;

        }
    },

    resetSelectedAddress: function () {
        var selectElement = $('#billing-address-select');
        if (selectElement) {
            selectElement.val('');
        }
    },

    save: function () {
        if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('billing');
        mySaveAddressBilling = true;
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },


    saveone: function () {
        if (Checkout.loadWaiting != false) return;
        Checkout.setLoadWaiting('billing');
        $.ajax({
            cache: false,
            url: this.saveOneUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStepOne,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    saveOrderInfo: function () {
        //if (Checkout.loadWaiting != false) return;

        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStepOrderInfo,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },
    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },
    nextStepOrderInfo: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }
            return false;
        }

        if (response.update_section_order) {
            $('#checkout-' + response.update_section_order.name + '-load').html(response.update_section_order.html);
        }
        if (response.update_section_shippingAddress) {
            $('#checkout-' + response.update_section_shippingAddress.name + '-load').html(response.update_section_shippingAddress.html);
        }
        if (response.update_section_selectedBillingAddress) {
            $('#BillingselectedAddress').html(response.update_section_selectedBillingAddress.html);
        }

        if (response.update_section_shipping) {
            $('#checkout-' + response.update_section_shipping.name + '-load').html(response.update_section_shipping.html);
        }


    },
    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

        if (response.update_section_order) {
            $('#checkout-' + response.update_section_order.name + '-load').html(response.update_section_order.html);
        }
        if (response.update_section_shippingAddress) {
            $('#checkout-' + response.update_section_shippingAddress.name + '-load').html(response.update_section_shippingAddress.html);
        }
        if (response.update_section_billingAddress) {
            $('#checkout-' + response.update_section_billingAddress.name + '-load').html(response.update_section_billingAddress.html);
        }
       
    },

    nextStepOne: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

        if (response.update_section_order) {
            $('#checkout-' + response.update_section_order.name + '-load').html(response.update_section_order.html);
        }
        if (response.update_section_shippingAddress) {
            $('#checkout-' + response.update_section_shippingAddress.name + '-load').html(response.update_section_shippingAddress.html);
        }
        if (response.update_section_billingAddress) {
            $('#checkout-' + response.update_section_billingAddress.name + '-load').html(response.update_section_billingAddress.html);
        }
        if (response.update_section_shipping) {
            $('#checkout-' + response.update_section_shipping.name + '-load').html(response.update_section_shipping.html);
        }

        if (response.oneok) {
            myAddressBilling = true;
        }
    }

};



var Shipping = {
    form: false,
    saveUrl: false,
    saveOneUrl: false,

    init: function (form, saveUrl, saveoneUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
        this.saveOneUrl = saveoneUrl;
    },

    newAddress: function (isNew) {
        if (isNew) {
            myAddressShipping = false;
            this.resetSelectedAddress();
            $('#shipping-new-address-form').show();
        } else {
            $('#shipping-new-address-form').hide();

            if (mySaveAddressShipping == false) {
                this.saveOrderInfo();
            }
            mySaveAddressShipping = false;
            myAddressShipping = true;
        }
    },

    resetSelectedAddress: function () {
        var selectElement = $('#shipping-address-select');
        if (selectElement) {
            selectElement.val('');
        }
    },

    save: function () {
        if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('shipping');
        myAddressShipping = false;
        mySaveAddressShipping = true;
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    saveOrderInfo: function () {
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStepOrderInfo,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    saveone: function () {
        if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('shipping');

        $.ajax({
            cache: false,
            url: this.saveOneUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStepOne,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStepOrderInfo: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }
            return false;
        }
        
        if (response.update_section_order) {
            $('#checkout-' + response.update_section_order.name + '-load').html(response.update_section_order.html);
        }
        if (response.update_section_shipping) {
            $('#checkout-' + response.update_section_shipping.name + '-load').html(response.update_section_shipping.html);
        }

    },
    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

        if (response.update_section_shippingAddress) {
            $('#checkout-' + response.update_section_shippingAddress.name + '-load').html(response.update_section_shippingAddress.html);
        }


        if (response.update_section_order) {
            $('#checkout-' + response.update_section_order.name + '-load').html(response.update_section_order.html);
        }
        if (response.update_section_shipping) {
            $('#checkout-' + response.update_section_shipping.name + '-load').html(response.update_section_shipping.html);
        }

        //Checkout.setStepResponse(response);
    },

    nextStepOne: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

        if (response.update_section_shippingAddress) {
            $('#checkout-' + response.update_section_shippingAddress.name + '-load').html(response.update_section_shippingAddress.html);
        }


        if (response.update_section_order) {
            $('#checkout-' + response.update_section_order.name + '-load').html(response.update_section_order.html);
        }
        if (response.update_section_shipping) {
            console.log('shipping response: about to update shipping methods');
            $('#checkout-' + response.update_section_shipping.name + '-load').html(response.update_section_shipping.html);
        }


        if (response.oneok) {
            myAddressShipping = true;
        }
        //Checkout.setStepResponse(response);
    }
};



var ShippingMethod = {
    form: false,
    saveUrl: false,

    init: function (form, saveUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
    },

    validate: function () {
        var methods = document.getElementsByName('shippingoption');
        if (methods.length == 0) {
            alert('Your order cannot be completed at this time as there is no shipping methods available for it. Please make necessary changes in your shipping address.');
            return false;
        }

        for (var i = 0; i < methods.length; i++) {
            if (methods[i].checked) {
                return true;
            }
        }
        alert('Please specify shipping method.');
        return false;
    },

    save: function (myvalue) {
        if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('shipping-method');

        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: { shippingmethod: myvalue },   //$(this.form).serialize(),
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
        //}
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }
        if (response.update_section_order) {
            $('#checkout-' + response.update_section_order.name + '-load').html(response.update_section_order.html).fadeIn('slow');


        }
        if (response.update_section_paymentInfo) {
            $('#checkout-' + response.update_section_paymentInfo.name + '-load').html(response.update_section_paymentInfo.html).fadeIn('slow');

        }
        //Checkout.setStepResponse(response);
    }
};



var PaymentMethod = {
    form: false,
    saveUrl: false,

    init: function (form, saveUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
    },

    validate: function () {
        var methods = document.getElementsByName('paymentmethod');
        if (methods.length == 0) {
            alert('Your order cannot be completed at this time as there is no payment methods available for it.');
            return false;
        }

        for (var i = 0; i < methods.length; i++) {
            if (methods[i].checked) {
                return true;
            }
        }
        alert('Please specify payment method.');
        return false;
    },

    save: function (myvalue) {
        if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('payment-method');

        var rewardpoints = false;
        if ($('#UseRewardPoints').attr('checked')) {
            rewardpoints = true;
        } else {
            rewardpoints = false;
        }

        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: { payment: myvalue, userewardpoints: rewardpoints },
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }
        if (response.update_section) {
            $('#checkout-' + response.update_section.name + '-load').html(response.update_section.html);
        }
        if (response.update_section_order) {
            $('#checkout-' + response.update_section_order.name + '-load').html(response.update_section_order.html).fadeIn('slow');

        }

    }
};



var PaymentInfo = {
    form: false,
    saveUrl: false,

    init: function (form, saveUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
    },

    save: function () {

        if (Checkout.loadWaiting != false) {
            alert("Please wait for finish other process !");
            return;
        }
        if (!myAddressBilling) {
            alert("Please save billing address !");
            return;
        }
        if (!myAddressShipping) {
            alert("Please save shipping address !");
            return;
        }

        //if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('confirm-order');

        //Checkout.setLoadWaiting('payment-info');
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

        Checkout.setStepResponse(response);
    }
};


var ConfirmOrder = {
    form: false,
    saveUrl: false,
    isSuccess: false,

    init: function (saveUrl, successUrl) {
        this.saveUrl = saveUrl;
        this.successUrl = successUrl;
    },

    save: function () {

        if (Checkout.loadWaiting != false) {
            alert("Please wait for finish other process !");
            return;
        }
        if (!myAddressBilling) {
            alert("Please save billing address !");
            return;
        }

        var checkBS = false;
        checkBS = $('#SameAsBillingAddress').is(':checked');
        var lbs = $('#SameAsBillingAddress').length;
        if (checkBS || lbs == 0)
            myAddressShipping = true;

        if (!myAddressShipping) {
            alert("Please save shipping address !");
            return;
        }
        Checkout.setLoadWaiting('confirm-order');

        var mydata = $(PaymentInfo.form).serialize() + '&' + $.param({ 'sameasbillingaddress': checkBS });

        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: mydata, //$(PaymentInfo.form).serialize(),
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    resetLoadWaiting: function (transport) {
        Checkout.setLoadWaiting(false, ConfirmOrder.isSuccess);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

        if (response.redirect) {
            ConfirmOrder.isSuccess = true;
            location.href = response.redirect;
            return;
        }
        if (response.success) {
            ConfirmOrder.isSuccess = true;
            window.location = ConfirmOrder.successUrl;
            return;
        }

        Checkout.setStepResponse(response);
    }
};


var DiscountBox = {
    form: false,
    saveUrl: false,
    removeUrl: false,

    init: function (form, saveUrl, removeUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
        this.removeUrl = removeUrl;
    },

    save: function (coupon) {
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: { "discountcouponcode": coupon },
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },
    remove: function () {
        $.ajax({
            cache: false,
            url: this.removeUrl,
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },

    ajaxFailure: function (response) {
        alert(response);
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.update_section) {
            $('.' + response.update_section.name).html(response.update_section.html);
        }
        if (response.update_section_totals) {
            $('.' + response.update_section_totals.name).html(response.update_section_totals.html);
        }
        return false;
    },

};


var GiftCardBox = {
    form: false,
    saveUrl: false,
    removeUrl: false,

    init: function (form, saveUrl, removeUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
        this.removeUrl = removeUrl;
    },

    save: function (giftcardcouponcode) {
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: { "giftcardcouponcode": giftcardcouponcode },
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },
    remove: function (giftcardcouponcode) {
        $.ajax({
            cache: false,
            url: this.removeUrl,
            data: { "giftcard": giftcardcouponcode },
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },

    ajaxFailure: function (response) {
        alert(response);
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.update_section) {
            $('.' + response.update_section.name).html(response.update_section.html);
        }
        if (response.update_section_totals) {
            $('.' + response.update_section_totals.name).html(response.update_section_totals.html);
        }
        return false;
    },

};


function removeGiftCard(giftcardcouponcode) {
    GiftCardBox.remove(giftcardcouponcode);
    return false;
};