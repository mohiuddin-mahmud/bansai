﻿using Nop.Web.Framework.Mvc;
using Nop.Web.Models.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NopDestek.Plugin.Misc.SingleCheckout.Models
{
    public partial class OnePageCheckoutModel : BaseNopModel
    {
        public string Theme { get; set; }
        public bool ShippingRequired { get; set; }
        public bool DisableBillingAddressCheckoutStep { get; set; }
        
    }
}
