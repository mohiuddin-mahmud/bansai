﻿

namespace NopDestek.Plugin.Misc.SingleCheckout.Models
{
    public class myUpdateSectionJsonModel
    {
        public string name { get; set; }
        public string html { get; set; }

        public string nameBillingAddress { get; set; }
        public string htmlBillingAddress { get; set; }

        public string nameShippingAddress { get; set; }
        public string htmlShippingAddress { get; set; }

        public string nameShippingMethod { get; set; }
        public string htmlShippingMethod { get; set; }

        public string namePaymentMethod { get; set; }
        public string htmlPaymentMethod { get; set; }

        public string namePaymentInfo { get; set; }
        public string htmlPaymentInfo { get; set; }

        public string nameOrderInfo { get; set; }
        public string htmlOrderInfo { get; set; }


        public bool saveAddress { get; set; }

        public bool oneok { get; set; }
        public bool oneerror { get; set; }
    }
}
