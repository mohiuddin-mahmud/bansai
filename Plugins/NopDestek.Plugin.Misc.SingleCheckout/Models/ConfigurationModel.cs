﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.Routing;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;
using Nop.Web.Models.Media;
using Nop.Web.Framework;
using System;

namespace NopDestek.Plugin.Misc.SingleCheckout.Models
{
    public class ConfigurationModel : BaseNopModel
    {

        [NopResourceDisplayName("Plugins.Misc.SingleCheckout.Admin.Fields.AllowSavedAddress")]
        public bool AllowSavedAddress { get; set; }

        [NopResourceDisplayName("Plugins.Misc.SingleCheckout.Admin.Fields.ValidateAddress")]
        public bool ValidateAddress { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Order.OnePageCheckoutEnabled")]
        public bool OnePageCheckout { get; set; }

    }
}
