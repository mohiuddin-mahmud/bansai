﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;
using Nop.Web.Models.Checkout;

namespace NopDestek.Plugin.Misc.SingleCheckout.Models
{
    public partial class SingleCheckoutShippingAddressModel : CheckoutShippingAddressModel
    {
        public bool SameAsBillingAddress { get; set; }
        public bool CanCheck { get; set; }
        public int idBillingAddress { get; set; }
    }
}
