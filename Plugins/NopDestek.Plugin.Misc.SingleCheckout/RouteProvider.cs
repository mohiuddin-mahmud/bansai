﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;

namespace NopDestek.Plugin.Misc.SingleCheckout
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            
            routes.MapRoute(
                "NopDestek.Plugin.Misc.SingleCheckout.InstallStringResources",
                "Admin/Plugins/SingleCheckout/Admin/InstallStringResources",
                new { controller = "SingleCheckout", action = "InstallStringResources" },
                new[] { "NopDestek.Plugin.Misc.SingleCheckout.Controllers" }
            ).DataTokens.Add("area", "admin");

            routes.MapLocalizedRoute("NopDestek.Plugin.Misc.SingleCheckout.OnePageCheckout",
                 "onepagecheckout/",
                 new { controller = "SingleCheckout", action = "OnePageCheckout" },
                 new[] { "NopDestek.Plugin.Misc.SingleCheckout.Controllers" }
            );
            routes.MapRoute("NopDestek.Plugin.Misc.SingleCheckout.Configure",
                "Plugins/SingleCheckout/Configure",
                new { controller = "SingleCheckout", action = "Configure" },
                new[] { "NopDestek.Plugin.Misc.SingleCheckout.Controllers" }
           );

            //shopping cart
            routes.MapLocalizedRoute("SingleCheckoutShoppingCart",
                            "onepagecheckout/",
                            new { controller = "SingleCheckout", action = "ShoppingCart" },
                            new[] { "NopDestek.Plugin.Misc.SingleCheckout.Controllers" });


        }
        public int Priority
        {
            get
            {
                return 10;
            }
        }
    }
}
