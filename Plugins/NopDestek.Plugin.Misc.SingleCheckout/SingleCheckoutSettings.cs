﻿using Nop.Core.Configuration;
using System;


namespace NopDestek.Plugin.Misc.SingleCheckout
{
    public class SingleCheckoutSettings : ISettings
    {
        public bool AllowSavedAddress { get; set; }
        public bool ValidateAddress { get; set; }
    }
}
